﻿#define _USE_MATH_DEFINES
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <cmath>
#include <iomanip>


auto readBytes(std::string fileName) {
	std::ifstream in(fileName);
	std::vector<char> allBytes;
	char byte;
	if (in.is_open()) {
		while (in.get(byte)) {
			allBytes.push_back(byte);
		}
	} else {
		exit(1);
	}
	in.close();
	return allBytes;
}

auto __analyzeSyntaxAndReturnLinesList(std::vector<char> allBytes) {
	auto lines = new std::vector<std::vector<std::vector<char>>>;
	auto words = new std::vector<std::vector<char>>;
	auto symbols = new std::vector<char>;
	bool isWord = false;

	char newLine = 10;
	char space = 32;

	for (char symbol : allBytes) {
		if (symbol == newLine) {
			words->push_back(*symbols);
			delete symbols;
			symbols = new std::vector<char>;
			lines->push_back(*words);
			delete words;
			words = new std::vector<std::vector<char>>;
			isWord = false;
		} else if ((isWord) && (symbol == space)) {
			words->push_back(*symbols);
			delete symbols;
			symbols = new std::vector<char>;
			isWord = false;
		} else if (symbol != space) {
			isWord = true;
			symbols->push_back(symbol);
		}
	}
	return lines;
}

double __getCoeffsNumeric(std::vector<std::vector<char>>* line, int number) {
	std::string numberBuilder = "";
	int digits = line->at(number).size();

	for (int digit = 0; digit < digits; digit++) {
		char symbol = (char)line->at(number).at(digit);

		if (symbol == 'D')
			numberBuilder += 'E';
		else
			numberBuilder += symbol;
	}
	double numeric = std::stod(numberBuilder);
	return numeric;
}

auto extractIonCoeffs(std::vector<char> allBytes, int lineNumber) {
	auto lines = __analyzeSyntaxAndReturnLinesList(allBytes);
	auto line = &lines->at(lineNumber);
	std::vector<double> coeffs;
	int amountOfCoeffs = 4;

	for (int coeff = 0; coeff < amountOfCoeffs; coeff++) {
		double numeric = __getCoeffsNumeric(line, coeff);
		coeffs.push_back(numeric);
	}
	delete lines;
	return coeffs;
}

double __getGpsTimeNumeric(std::vector<std::vector<std::vector<char>>>* lines, int observation) {
	std::string numberBuilder = "";
	int observationOffset = 7;
	int digits = lines->at(observation + observationOffset).at(0).size();

	for (int digit = 0; digit < digits; digit++) {
		char symbol = (char)lines->at(observation + observationOffset).at(0).at(digit);

		if (symbol == 'D')
			numberBuilder += 'E';
		else
			numberBuilder += symbol;
	}
	double numeric = std::stod(numberBuilder);
	return numeric;
}

auto getGpsTime(std::vector<char> allBytes, int amountOfObservations, char requiredSatelliteNumber1, char requiredSatelliteNumber2, int requiredSatelliteNumberSize) {
	auto lines = __analyzeSyntaxAndReturnLinesList(allBytes);
	std::vector<double> gpsTimeArray(amountOfObservations, 0);
	int startOfObservations = 8;
	int linesPerObservation = 8;
	int asciiStarts = 48;

	for (unsigned int observation = startOfObservations; observation < lines->size(); observation += linesPerObservation) {
		try {
			char satelliteNumber1 = lines->at(observation).at(0).at(0);
			char satelliteNumber2 = lines->at(observation).at(0).at(1);
			int satelliteNumberSize = lines->at(observation).at(0).size();
			int hourFirstNumber = (int)lines->at(observation).at(4).at(0) - asciiStarts;

			if ((lines->at(observation).at(4).size() == 1) &&
				(hourFirstNumber % 2 == 0) &&
				(satelliteNumber1 == requiredSatelliteNumber1) &&
				(satelliteNumber2 == requiredSatelliteNumber2) &&
				(satelliteNumberSize == requiredSatelliteNumberSize)) {
				double numeric = __getGpsTimeNumeric(lines, observation);
				gpsTimeArray.at(hourFirstNumber / 2) = numeric;
			} else {
				int hourSecondNumber = (int)lines->at(observation).at(4).at(1) - asciiStarts;
				std::string result = std::to_string(hourFirstNumber) + std::to_string(hourSecondNumber);
				int value = std::stoi(result);

				if ((value % 2 == 0) &&
					(satelliteNumber1 == requiredSatelliteNumber1) &&
					(satelliteNumber2 == requiredSatelliteNumber2) &&
					(satelliteNumberSize == requiredSatelliteNumberSize)) {
					double numeric = __getGpsTimeNumeric(lines, observation);
					gpsTimeArray.at(value / 2) = numeric;
				}
			}
		}
		catch (std::exception ignored) {}
	}
	delete lines;
	return gpsTimeArray;
}

auto getGpsTime(std::vector<char> allBytes, int amountOfObservations, char requiredSatelliteNumber, int requiredSatelliteNumberSize) {
	auto lines = __analyzeSyntaxAndReturnLinesList(allBytes);
	std::vector<double> gpsTimeArray(amountOfObservations, 0);
	int startOfObservations = 8;
	int linesPerObservation = 8;
	int asciiStarts = 48;

	for (unsigned int observation = startOfObservations; observation < lines->size(); observation += linesPerObservation) {
		try {
			char satelliteNumber = lines->at(observation).at(0).at(0);
			int satelliteNumberSize = lines->at(observation).at(0).size();
			int hourFirstNumber = (int)lines->at(observation).at(4).at(0) - asciiStarts;

			if ((lines->at(observation).at(4).size() == 1) &&
				(hourFirstNumber % 2 == 0) &&
				(satelliteNumber == requiredSatelliteNumber) &&
				(satelliteNumberSize == requiredSatelliteNumberSize)) {
				double numeric = __getGpsTimeNumeric(lines, observation);
				gpsTimeArray.at(hourFirstNumber / 2) = numeric;
			} else {
				int hourSecondNumber = (int)lines->at(observation).at(4).at(1) - asciiStarts;
				std::string result = std::to_string(hourFirstNumber) + std::to_string(hourSecondNumber);
				int value = std::stoi(result);

				if ((value % 2 == 0) &&
					(satelliteNumber == requiredSatelliteNumber) &&
					(satelliteNumberSize == requiredSatelliteNumberSize)) {
					double numeric = __getGpsTimeNumeric(lines, observation);
					gpsTimeArray.at(value / 2) = numeric;
				}
			}
		}
		catch (std::exception ignored) {}
	}
	delete lines;
	return gpsTimeArray;
}

int __getNumeric(std::vector<std::vector<std::vector<char>>>* lines, int line, int lineWithTec, int number) {
	std::string numberBuilder = "";
	int numberLength = lines->at(line + lineWithTec).at(number).size();

	for (int digit = 0; digit < numberLength; digit++) {
		char symbol = (char)lines->at(line + lineWithTec).at(number).at(digit);
		numberBuilder += symbol;
	}
	int numeric = std::stoi(numberBuilder);
	return numeric;
}

auto __getNumberLine(std::vector<std::vector<std::vector<char>>>* lines, int line, int lineWithTec) {
	std::vector<int> numbersLine;
	int numbersInRow = lines->at(line + lineWithTec).size();

	for (int number = 0; number < numbersInRow; number++) {
		int numeric = __getNumeric(lines, line, lineWithTec, number);
		numbersLine.push_back(numeric);
	}
	return numbersLine;
}

auto getTecArray(std::vector<char> allBytes, int firstLine, char requiredFirstLatDigit, char requiredSecondLatDigit, char requiredThirdLatDigit) {
	auto lines = __analyzeSyntaxAndReturnLinesList(allBytes);
	std::vector<std::vector<std::vector<int>>> tecArray;
	std::vector<std::vector<int>> tecPerLat;
	std::vector<int> numbersLine;
	int amountOfLinesWithTec = firstLine + 5575;
	for (int line = firstLine; line < amountOfLinesWithTec; line++) {
		try {
			char firstLatDigit = lines->at(line).at(0).at(0);
			char secondLatDigit = lines->at(line).at(0).at(1);
			char thirdLatDigit = lines->at(line).at(0).at(2);
			int linesWithTecPerLat = 5;

			if ((firstLatDigit == requiredFirstLatDigit) &&
				(secondLatDigit == requiredSecondLatDigit) &&
				(thirdLatDigit == requiredThirdLatDigit)) {
				std::vector<std::vector<int>> tecPerLat;
				for (int lineWithTec = 1; lineWithTec <= linesWithTecPerLat; lineWithTec++) {
					numbersLine = __getNumberLine(lines, line, lineWithTec);
					tecPerLat.push_back(numbersLine);
				}
				tecArray.push_back(tecPerLat);
			}
		}
		catch (std::exception ignored) {}
	}
	delete lines;
	return tecArray;
}

double __getTecuToMetersCoefficient() {
	double l1 = 1575420000;
	double oneTecUnit = 1E16;
	double coefficient = 40.3 / pow(l1, 2) * oneTecUnit;
	return coefficient;
}

double __getDelayInTecu(std::vector<double> weightMatrix, std::vector<int> tec) {
	double delay = 0;
	for (int interval = 0; interval < 4; interval++) {
		double weight = weightMatrix.at(interval);
		double rawTec = tec.at(interval);
		double tecInOneTecUnit = rawTec * 0.1;
		delay += (weight * tecInOneTecUnit);
	}
	return delay;
}

double __getDelayInMeters(std::vector<double> weightMatrix, std::vector<int> tec) {
	double tecuToMetersCoefficient = __getTecuToMetersCoefficient();
	double delayInTecu = __getDelayInTecu(weightMatrix, tec);
	double delayInMeters = delayInTecu * tecuToMetersCoefficient;
	return delayInMeters;
}

double __getEarthCenteredAngle(double elevationAngle) {
	double earthCenteredAngle = 0.0137 / (elevationAngle + 0.11) - 0.022;
	return earthCenteredAngle;
}

double __getIppLatitude(double latpp, double azimuth, double elevationAngle) {
	double earthCenteredAngle = __getEarthCenteredAngle(elevationAngle);
	double ippLatitude = latpp + earthCenteredAngle * cos(azimuth);
	if (ippLatitude > 0.416)
		ippLatitude = 0.416;
	else if (ippLatitude < -0.416)
		ippLatitude = -0.416;
	return ippLatitude;
}

double __getIppLongtitude(double latpp, double lonpp, double azimuth, double elevationAngle) {
	double earthCenteredAngle = __getEarthCenteredAngle(elevationAngle);
	double ippLatitude = __getIppLatitude(latpp, azimuth, elevationAngle);
	double ippLongtitude = lonpp + (earthCenteredAngle * sin(azimuth) / (cos(ippLatitude)));
	return ippLongtitude;
}

double __getIppGeomagneticLatitude(double latpp, double lonpp, double azimuth, double elevationAngle) {
	double ippLatitude = __getIppLatitude(latpp, azimuth, elevationAngle);
	double ippLongtitude = __getIppLongtitude(latpp, lonpp, azimuth, elevationAngle);
	double ippGeomagneticLatitude = ippLatitude + 0.064 * cos(ippLongtitude - 1.617);
	return ippGeomagneticLatitude;
}

double __getIppLocalTime(double latpp, double lonpp, double azimuth, double elevationAngle, double gpsTime) {
	double secondsInOneDay = 86400;
	double secondsInTwelveHours = 43200;
	double ippLongtitude = __getIppLongtitude(latpp, lonpp, azimuth, elevationAngle);
	double ippLocalTime = secondsInTwelveHours * ippLongtitude + gpsTime;
	while (ippLocalTime > secondsInOneDay)
		ippLocalTime -= secondsInOneDay;
	while (ippLocalTime < 0)
		ippLocalTime += secondsInOneDay;
	return ippLocalTime;
}

double __getIonosphericDelayAmplitude(double latpp, double lonpp, double azimuth, double elevationAngle, std::vector<double> alpha) {
	double ippGeomagneticLatitude = __getIppGeomagneticLatitude(latpp, lonpp, azimuth, elevationAngle);
	double amplitude = 0;
	for (int i = 0; i < 4; i++) {
		amplitude += (alpha.at(i) * pow(ippGeomagneticLatitude, i));
	}
	if (amplitude < 0)
		amplitude = 0;
	return amplitude;
}

double __getIonosphericDelayPeriod(double latpp, double lonpp, double azimuth, double elevationAngle, std::vector<double> beta) {
	double ippGeomagneticLatitude = __getIppGeomagneticLatitude(latpp, lonpp, azimuth, elevationAngle);
	double period = 0;
	for (int i = 0; i < 4; i++) {
		period += (beta.at(i) * pow(ippGeomagneticLatitude, i));
	}
	if (period < 72000)
		period = 72000;
	return period;
}

double __getIonosphericDelayPhase(double latpp, double lonpp, double azimuth, double elevationAngle, double gpsTime, std::vector<double> beta) {
	double ippLocalTime = __getIppLocalTime(latpp, lonpp, azimuth, elevationAngle, gpsTime);
	double ionosphericDelayPeriod = __getIonosphericDelayPeriod(latpp, lonpp, azimuth, elevationAngle, beta);
	double ionosphericDelayPhase = 2 * M_PI * (ippLocalTime - 50400) / ionosphericDelayPeriod;
	return ionosphericDelayPhase;
}

double __getSlantFactor(double elevationAngle) {
	double slantFactor = 1 + 16 * pow((0.53 - elevationAngle), 3);
	return slantFactor;
}

double __getKlobucharDelayInSeconds(double latpp, double lonpp, double azimuth, double elevationAngle, double gpsTime,
									std::vector<double> alpha, std::vector<double> beta) {
	double ionosphericDelayPhase = __getIonosphericDelayPhase(latpp, lonpp, azimuth, elevationAngle, gpsTime, beta);
	double ionosphericDelayAmplitude = __getIonosphericDelayAmplitude(latpp, lonpp, azimuth, elevationAngle, alpha);
	double slantFactor = __getSlantFactor(elevationAngle);
	double ionosphericTimeDelay = 0;
	if (abs(ionosphericDelayPhase) > 1.57)
		ionosphericTimeDelay = 5E-9 * slantFactor;
	else
		ionosphericTimeDelay = (5E-9 + ionosphericDelayAmplitude * (1 - pow(ionosphericDelayPhase, 2) / 2 + pow(ionosphericDelayPhase, 4) / 24)) * slantFactor;
	return ionosphericTimeDelay;
}

double getKlobucharDelayInMeters(double latpp, double lonpp, double azimuth, double elevationAngle, double gpsTime,
								 std::vector<double> alpha, std::vector<double> beta) {
	double delayInSeconds = __getKlobucharDelayInSeconds(latpp, lonpp, azimuth, elevationAngle, gpsTime, alpha, beta);
	double speedOfLight = 2.99792458 * 1E8;
	double delayInMeters = delayInSeconds * speedOfLight;
	return delayInMeters;
}

int __getPos(int lonFirst, int dlon, int tecValuesPerLine, int row, int lon) {
	int number = int(abs(((lonFirst - lon) / dlon)) - (row * tecValuesPerLine));
	return number;
}

int __getRow(int lonFirst, int dlon, int tecValuesPerLine, int lon) {
	int row = int(abs((lonFirst - lon) / (tecValuesPerLine * dlon)));
	return row;
}

auto getIonosphericDelays(std::vector<std::vector<std::vector<int>>> tecA1,
	std::vector<std::vector<std::vector<int>>> tecA2,
	std::vector<std::vector<std::vector<int>>> tecA3,
	std::vector<std::vector<std::vector<int>>> tecA4,
	int lon1, int lon2, int lon3, int lon4,
	std::vector<double> weightMatrix, int amountOfObservations) {
	int lonFirst = -180;
	int dlon = 5;
	int tecValuesPerLine = 16;
	std::vector<double> delays;

	int rowA1 = __getRow(lonFirst, dlon, tecValuesPerLine, lon1);
	int rowA2 = __getRow(lonFirst, dlon, tecValuesPerLine, lon2);
	int rowA3 = __getRow(lonFirst, dlon, tecValuesPerLine, lon3);
	int rowA4 = __getRow(lonFirst, dlon, tecValuesPerLine, lon4);

	int posA1 = __getPos(lonFirst, dlon, tecValuesPerLine, rowA1, lon1);
	int posA2 = __getPos(lonFirst, dlon, tecValuesPerLine, rowA2, lon2);
	int posA3 = __getPos(lonFirst, dlon, tecValuesPerLine, rowA3, lon3);
	int posA4 = __getPos(lonFirst, dlon, tecValuesPerLine, rowA4, lon4);

	for (int observation = 0; observation < amountOfObservations; observation++) {
		int a1 = tecA1.at(observation).at(rowA1).at(posA1);
		int a2 = tecA2.at(observation).at(rowA2).at(posA2);
		int a3 = tecA3.at(observation).at(rowA3).at(posA3);
		int a4 = tecA4.at(observation).at(rowA4).at(posA4);

		std::vector<int> tecOnAngles;
		tecOnAngles.push_back(a1);
		tecOnAngles.push_back(a2);
		tecOnAngles.push_back(a3);
		tecOnAngles.push_back(a4);

		double delay = __getDelayInMeters(weightMatrix, tecOnAngles);
		delays.push_back(delay);
	}
	return delays;
}

auto getKlobuchar(double latpp, double lonpp, double elevationAngle, double azimuth,
	std::vector<double> alpha, std::vector<double> beta,
	std::vector<double> gpsTime, int amountOfObservations) {
	std::vector<double> models;
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double time = gpsTime.at(observation);
		double model = getKlobucharDelayInMeters(latpp, lonpp, azimuth, elevationAngle, time, alpha, beta);
		models.push_back(model);
	}
	return models;
}

int main() {
	setlocale(LC_ALL, "rus");
	int amountOfObservations = 12;

	double halfCircle = 180;

	double latpp = 56 / halfCircle;
	double lonpp = 92 / halfCircle;

	double lon1 = 85 / halfCircle;
	double lon2 = 95 / halfCircle;
	double lat1 = 50 / halfCircle;
	double lat2 = 60 / halfCircle;

	double xpp = (lonpp - lon1) / (lon2 - lon1);
	double ypp = (latpp - lat1) / (lat2 - lat1);

	std::vector<double> weightMatrix;
	weightMatrix.at(0) = xpp * ypp;
	weightMatrix.at(1) = (1 - xpp) * ypp;
	weightMatrix.at(2) = (1 - xpp) * (1 - ypp);
	weightMatrix.at(3) = xpp * (1 - ypp);

	auto fileBytesEphemeris = readBytes("C:\\Users\\GVOZDEV\\Desktop\\cplusplusport\\cPlusPlusPort\\Var3FileNoOop\\Var3FileNoOop\\resources\\brdc0010.18n");
	auto alpha = extractIonCoeffs(fileBytesEphemeris, 3);
	auto beta = extractIonCoeffs(fileBytesEphemeris, 4);
	auto gpsTime = getGpsTime(fileBytesEphemeris, amountOfObservations, '7', 1);

	auto fileBytesForecast = readBytes("C:\\Users\\GVOZDEV\\Desktop\\cplusplusport\\cPlusPlusPort\\Var3FileNoOop\\Var3FileNoOop\\resources\\igrg0010.18i");
	auto forecastA1 = getTecArray(fileBytesForecast, 304, '6', '5', '.');
	auto forecastA2 = getTecArray(fileBytesForecast, 304, '6', '5', '.');
	auto forecastA3 = getTecArray(fileBytesForecast, 304, '5', '5', '.');
	auto forecastA4 = getTecArray(fileBytesForecast, 304, '5', '5', '.');

	auto fileBytesPrecise = readBytes("C:\\Users\\GVOZDEV\\Desktop\\cplusplusport\\cPlusPlusPort\\Var3FileNoOop\\Var3FileNoOop\\resources\\igsg0010.18i");
	auto preciseA1 = getTecArray(fileBytesPrecise, 394, '6', '5', '.');
	auto preciseA2 = getTecArray(fileBytesPrecise, 394, '6', '5', '.');
	auto preciseA3 = getTecArray(fileBytesPrecise, 394, '5', '5', '.');
	auto preciseA4 = getTecArray(fileBytesPrecise, 394, '5', '5', '.');

	auto forecastDelays = getIonosphericDelays(forecastA1, forecastA2, forecastA3, forecastA4, 55, 45, 45, 55, weightMatrix, amountOfObservations);
	auto preciseDelays = getIonosphericDelays(preciseA1, preciseA2, preciseA3, preciseA4, 55, 45, 45, 55, weightMatrix, amountOfObservations);

	double elevationAngle = 90 / halfCircle;
	double azimuth = 0;
	auto klobucharDelays = getKlobuchar(latpp, lonpp, elevationAngle, azimuth, alpha, beta, gpsTime, amountOfObservations);

	std::cout << "igrg\t\tigsg\t\tklobuchar" << std::endl;
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double forecastValue = forecastDelays.at(observation);
		double preciseValue = preciseDelays.at(observation);
		double klobucharValue = klobucharDelays.at(observation);

		std::cout << std::fixed << std::setprecision(3) <<
			forecastValue << "\t\t" << preciseValue << "\t\t" << klobucharValue << std::endl;
	}
}