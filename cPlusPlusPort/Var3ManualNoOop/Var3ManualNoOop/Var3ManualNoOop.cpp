﻿#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <iomanip>


double __getTecuToMetersCoefficient() {
	double l1 = 1575420000;
	double oneTecUnit = 1E16;
	double coefficient = 40.3 / pow(l1, 2) * oneTecUnit;
	return coefficient;
}

double __getDelayInTecu(std::vector<double> weightMatrix, std::vector<int> tec) {
	double delay = 0;
	for (int interval = 0; interval < 4; interval++) {
		double weight = weightMatrix.at(interval);
		double rawTec = tec.at(interval);
		double tecInOneTecUnit = rawTec * 0.1;
		delay += (weight * tecInOneTecUnit);
	}
	return delay;
}

double getDelayInMeters(std::vector<double> weightMatrix, std::vector<int> tec) {
	double tecuToMetersCoefficient = __getTecuToMetersCoefficient();
	double delayInTecu = __getDelayInTecu(weightMatrix, tec);
	double delayInMeters = delayInTecu * tecuToMetersCoefficient;
	return delayInMeters;
}

double __getEarthCenteredAngle(double elevationAngle) {
	double earthCenteredAngle = 0.0137 / (elevationAngle + 0.11) - 0.022;
	return earthCenteredAngle;
}

double __getIppLatitude(double latpp, double azimuth, double elevationAngle) {
	double earthCenteredAngle = __getEarthCenteredAngle(elevationAngle);
	double ippLatitude = latpp + earthCenteredAngle * cos(azimuth);
	if (ippLatitude > 0.416)
		ippLatitude = 0.416;
	else if (ippLatitude < -0.416)
		ippLatitude = -0.416;
	return ippLatitude;
}

double __getIppLongtitude(double latpp, double lonpp, double azimuth, double elevationAngle) {
	double earthCenteredAngle = __getEarthCenteredAngle(elevationAngle);
	double ippLatitude = __getIppLatitude(latpp, azimuth, elevationAngle);
	double ippLongtitude = lonpp + (earthCenteredAngle * sin(azimuth) / (cos(ippLatitude)));
	return ippLongtitude;
}

double __getIppGeomagneticLatitude(double latpp, double lonpp, double azimuth, double elevationAngle) {
	double ippLatitude = __getIppLatitude(latpp, azimuth, elevationAngle);
	double ippLongtitude = __getIppLongtitude(latpp, lonpp, azimuth, elevationAngle);
	double ippGeomagneticLatitude = ippLatitude + 0.064 * cos(ippLongtitude - 1.617);
	return ippGeomagneticLatitude;
}

double __getIppLocalTime(double latpp, double lonpp, double azimuth, double elevationAngle, double gpsTime) {
	double secondsInOneDay = 86400;
	double secondsInTwelveHours = 43200;
	double ippLongtitude = __getIppLongtitude(latpp, lonpp, azimuth, elevationAngle);
	double ippLocalTime = secondsInTwelveHours * ippLongtitude + gpsTime;
	while (ippLocalTime > secondsInOneDay)
		ippLocalTime -= secondsInOneDay;
	while (ippLocalTime < 0)
		ippLocalTime += secondsInOneDay;
	return ippLocalTime;
}

double __getIonosphericDelayAmplitude(double latpp, double lonpp, double azimuth, double elevationAngle, std::vector<double> alpha) {
	double ippGeomagneticLatitude = __getIppGeomagneticLatitude(latpp, lonpp, azimuth, elevationAngle);
	double amplitude = 0;
	for (int i = 0; i < 4; i++) {
		amplitude += (alpha.at(i) * pow(ippGeomagneticLatitude, i));
	}
	if (amplitude < 0)
		amplitude = 0;
	return amplitude;
}

double __getIonosphericDelayPeriod(double latpp, double lonpp, double azimuth, double elevationAngle, std::vector<double> beta) {
	double ippGeomagneticLatitude = __getIppGeomagneticLatitude(latpp, lonpp, azimuth, elevationAngle);
	double period = 0;
	for (int i = 0; i < 4; i++) {
		period += (beta.at(i) * pow(ippGeomagneticLatitude, i));
	}
	if (period < 72000)
		period = 72000;
	return period;
}

double __getIonosphericDelayPhase(double latpp, double lonpp, double azimuth, double elevationAngle, double gpsTime,
								  std::vector<double> beta) {
	double ippLocalTime = __getIppLocalTime(latpp, lonpp, azimuth, elevationAngle, gpsTime);
	double ionosphericDelayPeriod = __getIonosphericDelayPeriod(latpp, lonpp, azimuth, elevationAngle, beta);
	double ionosphericDelayPhase = 2 * M_PI * (ippLocalTime - 50400) / ionosphericDelayPeriod;
	return ionosphericDelayPhase;
}

double __getSlantFactor(double elevationAngle) {
	double slantFactor = 1 + 16 * pow((0.53 - elevationAngle), 3);
	return slantFactor;
}

double __getKlobucharDelayInSeconds(double latpp, double lonpp, double azimuth, double elevationAngle, double gpsTime,
									std::vector<double> alpha, std::vector<double> beta) {
	double ionosphericDelayPhase = __getIonosphericDelayPhase(latpp, lonpp, azimuth, elevationAngle, gpsTime, beta);
	double ionosphericDelayAmplitude = __getIonosphericDelayAmplitude(latpp, lonpp, azimuth, elevationAngle, alpha);
	double slantFactor = __getSlantFactor(elevationAngle);
	double ionosphericTimeDelay = 0;
	if (abs(ionosphericDelayPhase) > 1.57)
		ionosphericTimeDelay = 5E-9 * slantFactor;
	else
		ionosphericTimeDelay = (5E-9 + ionosphericDelayAmplitude * (1 - pow(ionosphericDelayPhase, 2) / 2 + pow(ionosphericDelayPhase, 4) / 24)) * slantFactor;
	return ionosphericTimeDelay;
}

double getKlobucharDelayInMeters(double latpp, double lonpp, double azimuth, double elevationAngle, double gpsTime,
								 std::vector<double> alpha, std::vector<double> beta) {
	double delayInSeconds = __getKlobucharDelayInSeconds(latpp, lonpp, azimuth, elevationAngle, gpsTime, alpha, beta);
	double speedOfLight = 2.99792458 * 1E8;
	double delayInMeters = delayInSeconds * speedOfLight;
	return delayInMeters;
}

std::vector<double> createIonosphericDelays(std::vector<int> tecA1, std::vector<int> tecA2, std::vector<int> tecA3,
											std::vector<int> tecA4, std::vector<double> weightMatrix, int amountOfObservations) {
	std::vector<double> delays;
	for (int observation = 0; observation < amountOfObservations; observation++) {
		int a1 = tecA1.at(observation);
		int a2 = tecA2.at(observation);
		int a3 = tecA3.at(observation);
		int a4 = tecA4.at(observation);

		std::vector<int> tec;
		tec.push_back(a1);
		tec.push_back(a2);
		tec.push_back(a3);
		tec.push_back(a4);

		double delay = getDelayInMeters(weightMatrix, tec);
		delays.push_back(delay);
	}
	return delays;
}

std::vector<double> createKlobuchar(double latpp, double lonpp, double azimuth, double elevationAngle, double amountOfObservations,
									std::vector<double> alpha, std::vector<double> beta, std::vector<double> gpsTime) {
	std::vector<double> models;
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double time = gpsTime.at(observation);
		double model = getKlobucharDelayInMeters(latpp, lonpp, azimuth, elevationAngle, time, alpha, beta);
		models.push_back(model);
	}
	return models;
}


int main() {
	setlocale(LC_ALL, "rus");
	const int amountOfObservations = 12;

	double halfCircle = 180;

	double latpp = 48 / halfCircle;
	double lonpp = 135 / halfCircle;

	double lon1 = 130 / halfCircle;
	double lon2 = 140 / halfCircle;
	double lat1 = 45 / halfCircle;
	double lat2 = 55 / halfCircle;

	double xpp = (lonpp - lon1) / (lon2 - lon1);
	double ypp = (latpp - lat1) / (lat2 - lat1);

	std::vector<double> weightMatrix;
	weightMatrix.at(0) = xpp * ypp;
	weightMatrix.at(1) = (1 - xpp) * ypp;
	weightMatrix.at(2) = (1 - xpp) * (1 - ypp);
	weightMatrix.at(3) = xpp * (1 - ypp);

	std::vector<double> alpha { 0.7451E-08, -0.1490E-07, -0.5960E-07,  0.1192E-06 };
	std::vector<double> beta { 0.9216E+05, -0.1147E+06, -0.1311E+06, 0.7209E+06 };
	std::vector<double> gpsTime {
		80496.0, 90270.0, 93630.0, 100830.0, 108030.0, 0.0, 124230.0, 129630.0, 136800.0, 144000.0, 151200.0, 158400.0
	};

	std::vector<int> tecA1forecast { 72, 81, 86, 73, 47, 52, 55, 57, 58, 56, 46, 38 };
	std::vector<int> tecA2forecast { 53, 84, 86, 80, 59, 54, 54, 60, 67, 57, 54, 45 };
	std::vector<int> tecA3forecast { 80, 108, 92, 98, 79, 73, 76, 76, 73, 66, 65, 55 };
	std::vector<int> tecA4forecast { 88, 108, 91, 97, 70, 72, 73, 74, 69, 63, 54, 55 };

	std::vector<int> tecA1precise { 69, 78, 80, 64, 36, 42, 42, 52, 51, 49, 42, 37 };
	std::vector<int> tecA2precise { 51, 80, 81, 68, 45, 47, 41, 51, 56, 51, 49, 41 };
	std::vector<int> tecA3precise { 63, 97, 80, 87, 60, 55, 58, 59, 57, 58, 50, 39 };
	std::vector<int> tecA4precise { 70, 98, 74, 82, 48, 51, 52, 56, 48, 49, 35, 39 };

	std::vector<double> forecastValues = createIonosphericDelays(tecA1forecast, tecA2forecast, tecA3forecast, tecA4forecast, weightMatrix, amountOfObservations);
	std::vector<double> preciseValues = createIonosphericDelays(tecA1precise, tecA2precise, tecA3precise, tecA4precise, weightMatrix, amountOfObservations);

	double elevationAngle = 90 / halfCircle;
	int azimuth = 0;
	std::vector<double> klobucharValues = createKlobuchar(latpp, lonpp, azimuth, elevationAngle, amountOfObservations, alpha, beta, gpsTime);

	std::cout << "igrg\t\tigsg\t\tklobuchar" << std::endl;
	for (int observation = 0; observation < amountOfObservations; observation++) {
		double forecastValue = forecastValues.at(observation);
		double preciseValue = preciseValues.at(observation);
		double klobucharValue = klobucharValues.at(observation);

		std::cout << std::fixed << std::setprecision(3) <<
			forecastValue << "\t\t" << preciseValue << "\t\t" << klobucharValue << std::endl;
	}
}