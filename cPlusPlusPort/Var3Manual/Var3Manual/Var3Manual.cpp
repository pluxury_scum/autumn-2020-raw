﻿#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <iomanip>


class UserGeo {
public:
    UserGeo() {
        double halfCircle = 180;
        latpp = 71.691643 / halfCircle;
        lonpp = 128.865978 / halfCircle;
    }

    double getLatpp() {
        return latpp;
    }

    double getLonpp() {
        return lonpp;
    }

private:
    double latpp;
    double lonpp;
};


class IgpGeo {
public:
    IgpGeo() {
        double halfCircle = 180;
        lon1 = 125 / halfCircle;
        lon2 = 130 / halfCircle;
        lat1 = 70 / halfCircle;
        lat2 = 72.5 / halfCircle;
    }

    double getLon1() {
        return lon1;
    }

    double getLon2() {
        return lon2;
    }

    double getLat1() {
        return lat1;
    }

    double getLat2() {
        return lat2;
    }

private:
    double lon1;
    double lon2;
    double lat1;
    double lat2;
};


class AxisGeo {
public:
    AxisGeo(double lonpp, double lon1, double lon2, double latpp, double lat1, double lat2) {
        xpp = (lonpp - lon1) / (lon2 - lon1);
        ypp = (latpp - lat1) / (lat2 - lat1);
    }

    double getXpp() {
        return xpp;
    }

    double getYpp() {
        return ypp;
    }

private:
    double xpp;
    double ypp;
};


class WeightMatrix {
public:
    WeightMatrix(AxisGeo& axisGeo) {
        this->axisGeo = &axisGeo;
        xpp = axisGeo.getXpp();
        ypp = axisGeo.getYpp();
        weights.at(0) = xpp * ypp;
        weights.at(1) = (1 - xpp) * ypp;
        weights.at(2) = (1 - xpp) * (1 - ypp);
        weights.at(3) = xpp * (1 - ypp);
    }

    double getWeightAt(int pos) {
        return weights.at(pos);
    }

private:
    AxisGeo* axisGeo;
    std::vector<double> weights;
    double xpp;
    double ypp;
};


class IonCoefficients {
public:
    IonCoefficients(std::vector<double> coefficients) {
        for (int coefficient = 0; coefficient < 4; coefficient++) {
            double coeff = coefficients.at(coefficient);
            this->coefficients.push_back(coeff);
        }
    }

    double getCoefficientAt(int pos) {
        return coefficients.at(pos);
    }

private:
    std::vector<double> coefficients;
};


class Tec {
public:
    Tec(std::vector<int> tec) {
        this->tec = tec;
    }

    double getTecAt(int pos) {
        return tec.at(pos);
    }

private:
    std::vector<int> tec;
};


class GpsTime {
public:
    GpsTime(std::vector<double> gpsTime, int amountOfObservations) {
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double time = gpsTime.at(observation);
            this->gpsTime.push_back(time);
        }
    }

    double getGpsTimeAt(int pos) {
        return gpsTime.at(pos);
    }

private:
    std::vector<double> gpsTime;
};


class IonosphericDelay {
public:
    IonosphericDelay(WeightMatrix& weightMatrix, Tec& tec) {
        this->weightMatrix = &weightMatrix;
        this->tec = &tec;
    }

    double getDelayInMeters() {
        double tecuToMetersCoefficient = getTecuToMetersCoefficient();
        double delayInTecu = getDelayInTecu();
        double delayInMeters = delayInTecu * tecuToMetersCoefficient;
        return delayInMeters;
    }

private:
    WeightMatrix* weightMatrix;
    Tec* tec;

    double getTecuToMetersCoefficient() {
        double l1 = 1575420000;
        double oneTecUnit = 1E16;
        double coefficient = 40.3 / pow(l1, 2) * oneTecUnit;
        return coefficient;
    }

    double getDelayInTecu() {
        double delay = 0;
        for (int interval = 0; interval < 4; interval++) {
            double weight = weightMatrix->getWeightAt(interval);
            double rawTec = tec->getTecAt(interval);
            double tecInOneTecUnit = rawTec * 0.1;
            delay += (weight * tecInOneTecUnit);
        }
        return delay;
    }
};


class KlobucharModel {
public:
    KlobucharModel(double gpsTime, IonCoefficients& alpha, IonCoefficients& beta, UserGeo& userGeo) {
        double halfCircle = 180;
        this->gpsTime = gpsTime;
        this->elevationAngle = 90 / halfCircle;
        this->azimuth = 0;
        this->alpha = &alpha;
        this->beta = &beta;
        this->userGeo = &userGeo;
    }

    double getKlobucharDelayInMeters() {
        double delayInSeconds = getKlobucharDelayInSeconds();
        double speedOfLight = 2.99792458 * 1E8;
        double delayInMeters = delayInSeconds * speedOfLight;
        return delayInMeters;
    }

private:
    IonCoefficients* alpha;
    IonCoefficients* beta;
    UserGeo* userGeo;
    double elevationAngle, azimuth, gpsTime;

    double getEarthCenteredAngle() {
        double earthCenteredAngle = 0.0137 / (elevationAngle + 0.11) - 0.022;
        return earthCenteredAngle;
    }

    double getIppLatitude() {
        double latpp = userGeo->getLatpp();
        double earthCenteredAngle = getEarthCenteredAngle();
        double ippLatitude = latpp + earthCenteredAngle * cos(azimuth);
        if (ippLatitude > 0.416)
            ippLatitude = 0.416;
        else if (ippLatitude < -0.416)
            ippLatitude = -0.416;
        return ippLatitude;
    }

    double getIppLongtitude() {
        double lonpp = userGeo->getLonpp();
        double earthCenteredAngle = getEarthCenteredAngle();
        double ippLatitude = getIppLatitude();
        double ippLongtitude = lonpp + (earthCenteredAngle * sin(azimuth) / (cos(ippLatitude)));
        return ippLongtitude;
    }

    double getIppGeomagneticLatitude() {
        double ippLatitude = getIppLatitude();
        double ippLongtitude = getIppLongtitude();
        double ippGeomagneticLatitude = ippLatitude + 0.064 * cos(ippLongtitude - 1.617);
        return ippGeomagneticLatitude;
    }

    double getIppLocalTime() {
        double secondsInOneDay = 86400;
        double secondsInTwelveHours = 43200;
        double ippLongtitude = getIppLongtitude();
        double ippLocalTime = secondsInTwelveHours * ippLongtitude + gpsTime;
        while (ippLocalTime > secondsInOneDay)
            ippLocalTime -= secondsInOneDay;
        while (ippLocalTime < 0)
            ippLocalTime += secondsInOneDay;
        return ippLocalTime;
    }

    double getIonosphericDelayAmplitude() {
        double ippGeomagneticLatitude = getIppGeomagneticLatitude();
        double amplitude = 0;
        for (int i = 0; i < 4; i++) {
            double alphaValue = alpha->getCoefficientAt(i);
            amplitude += (alphaValue * pow(ippGeomagneticLatitude, i));
        }
        if (amplitude < 0)
            amplitude = 0;
        return amplitude;
    }

    double getIonosphericDelayPeriod() {
        double ippGeomagneticLatitude = getIppGeomagneticLatitude();
        double period = 0;
        for (int i = 0; i < 4; i++) {
            double betaValue = beta->getCoefficientAt(i);
            period += (betaValue * pow(ippGeomagneticLatitude, i));
        }
        if (period < 72000)
            period = 72000;
        return period;
    }

    double getIonosphericDelayPhase() {
        double ippLocalTime = getIppLocalTime();
        double ionosphericDelayPeriod = getIonosphericDelayPeriod();
        double ionosphericDelayPhase = 2 * M_PI * (ippLocalTime - 50400) / ionosphericDelayPeriod;
        return ionosphericDelayPhase;
    }

    double getSlantFactor() {
        double slantFactor = 1 + 16 * pow((0.53 - elevationAngle), 3);
        return slantFactor;
    }

    double getKlobucharDelayInSeconds() {
        double ionosphericDelayPhase = getIonosphericDelayPhase();
        double ionosphericDelayAmplitude = getIonosphericDelayAmplitude();
        double slantFactor = getSlantFactor();
        double ionosphericTimeDelay = 0;
        if (abs(ionosphericDelayPhase) > 1.57)
            ionosphericTimeDelay = 5E-9 * slantFactor;
        else
            ionosphericTimeDelay = (5E-9 + ionosphericDelayAmplitude * (1 - pow(ionosphericDelayPhase, 2) / 2 + pow(ionosphericDelayPhase, 4) / 24)) * slantFactor;
        return ionosphericTimeDelay;
    }
};


class IonosphericDelaysFactory {
public:
    IonosphericDelaysFactory(WeightMatrix& weightMatrix, int amountOfObservations) {
        this->amountOfObservations = amountOfObservations;
        this->weightMatrix = &weightMatrix;
    }

    std::vector<IonosphericDelay> createDelays(std::vector<int> tecA1, std::vector<int> tecA2, std::vector<int> tecA3,
                                               std::vector<int> tecA4) {
        std::vector<IonosphericDelay> delays;
        for (int observation = 0; observation < amountOfObservations; observation++) {
            int a1 = tecA1.at(observation);
            int a2 = tecA2.at(observation);
            int a3 = tecA3.at(observation);
            int a4 = tecA4.at(observation);

            std::vector<int> tecArray;
            tecArray.push_back(a1);
            tecArray.push_back(a2);
            tecArray.push_back(a3);
            tecArray.push_back(a4);

            Tec* tec = new Tec(tecArray);
            IonosphericDelay* delay = new IonosphericDelay(*weightMatrix, *tec);
            delays.push_back(*delay);
        }
        return delays;
    }

private:
    int amountOfObservations;
    WeightMatrix* weightMatrix;
};


class KlobucharDelaysFactory {
public:
    KlobucharDelaysFactory(GpsTime& gpsTime, IonCoefficients& alpha, IonCoefficients& beta, UserGeo& userGeo, int amountOfObservations) {
        this->amountOfObservations = amountOfObservations;
        this->gpsTime = &gpsTime;
        this->alpha = &alpha;
        this->beta = &beta;
        this->userGeo = &userGeo;
    }

    std::vector<KlobucharModel> createKlobuchar() {
        std::vector<KlobucharModel> models;
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double time = gpsTime->getGpsTimeAt(observation);
            KlobucharModel* model = new KlobucharModel(time, *alpha, *beta, *userGeo);
            models.push_back(*model);
        }
        return models;
    }

private:
    int amountOfObservations;
    GpsTime* gpsTime;
    IonCoefficients* alpha;
    IonCoefficients* beta;
    UserGeo* userGeo;
};


class ConsoleOutput {
public:
    ConsoleOutput(std::vector<IonosphericDelay> forecastValues, std::vector<IonosphericDelay> preciseValues,
                  std::vector<KlobucharModel> klobucharValues, int amountOfObservations) {
        this->forecastValues = forecastValues;
        this->preciseValues = preciseValues;
        this->klobucharValues = klobucharValues;
        this->amountOfObservations = amountOfObservations;
    }

    void printDelays() {
        std::cout << "igrg\t\tigsg\t\tklobuchar" << std::endl;
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double forecastValue = forecastValues.at(observation).getDelayInMeters();
            double preciseValue = preciseValues.at(observation).getDelayInMeters();
            double klobucharValue = klobucharValues.at(observation).getKlobucharDelayInMeters();
            std::cout << std::fixed << std::setprecision(3) <<
                forecastValue << "\t\t" << preciseValue << "\t\t" << klobucharValue << std::endl;
        }
    }

private:
    std::vector<IonosphericDelay> forecastValues;
    std::vector<IonosphericDelay> preciseValues;
    std::vector<KlobucharModel> klobucharValues;
    int amountOfObservations;
};


int main() {
    setlocale(LC_ALL, "rus");
    const int amountOfObservations = 12;

    UserGeo userGeo;
    IgpGeo igpGeo;

    double lonpp = userGeo.getLonpp();
    double lon1 = igpGeo.getLon1();
    double lon2 = igpGeo.getLon2();
    double latpp = userGeo.getLatpp();
    double lat1 = igpGeo.getLat1();
    double lat2 = igpGeo.getLat2();

    AxisGeo axisGeo(lonpp, lon1, lon2, latpp, lat1, lat2);
    WeightMatrix weightMatrix(axisGeo);

    std::vector<double> alphaArray { 0.7451E-08, -0.1490E-07, -0.5960E-07,  0.1192E-06 };
    std::vector<double> betaArray { 0.9216E+05, -0.1147E+06, -0.1311E+06, 0.7209E+06 };
    std::vector<double> gpsArray {
        80496.0, 90270.0, 93630.0, 100830.0, 108030.0, 0.0, 124230.0, 129630.0, 136800.0, 144000.0, 151200.0, 158400.0
    };

    IonCoefficients alpha(alphaArray);
    IonCoefficients beta(betaArray);
    GpsTime gpsTime(gpsArray, amountOfObservations);

    std::vector<int> tecA1forecast { 31, 32, 42, 39, 30, 31, 26, 25, 19, 14, 14, 14 };
    std::vector<int> tecA2forecast { 31, 32, 41, 39, 30, 26, 26, 24, 18, 14, 14, 16 };
    std::vector<int> tecA3forecast { 24, 35, 49, 45, 31, 26, 26, 26, 21, 16, 13, 17 };
    std::vector<int> tecA4forecast { 25, 36, 50, 43, 30, 25, 26, 26, 18, 14, 13, 15 };

    std::vector<int> tecA1precise { 25, 28, 36, 35, 24, 23, 19, 30, 15, 9, 5, 11 };
    std::vector<int> tecA2precise { 26, 27, 35, 36, 25, 25, 19, 31, 17, 9, 5, 12 };
    std::vector<int> tecA3precise { 21, 29, 43, 39, 22, 21, 21, 28, 19, 10, 4, 13 };
    std::vector<int> tecA4precise { 20, 30, 43, 37, 20, 19, 21, 28, 17, 11, 4, 11 };

    IonosphericDelaysFactory ionosphericDelaysFactory(weightMatrix, amountOfObservations);
    KlobucharDelaysFactory klobucharDelaysFactory(gpsTime, alpha, beta, userGeo, amountOfObservations);

    std::vector<IonosphericDelay> forecastValues = ionosphericDelaysFactory.createDelays(tecA1forecast, tecA2forecast, tecA3forecast, tecA4forecast);
    std::vector<IonosphericDelay> preciseValues = ionosphericDelaysFactory.createDelays(tecA1precise, tecA2precise, tecA3precise, tecA4precise);
    std::vector<KlobucharModel> klobucharValues = klobucharDelaysFactory.createKlobuchar();

    ConsoleOutput consoleOutput(forecastValues, preciseValues, klobucharValues, amountOfObservations);
    consoleOutput.printDelays();
}