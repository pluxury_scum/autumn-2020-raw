import math
import matplotlib.pyplot as plt
import numpy as np


def readBytes(fileName: str) -> bytes:
    with open(fileName, "rb") as file:
        allBytes: bytes = file.read()
    return allBytes


def __analyzeSyntaxAndReturnLinesList(allBytes: bytes) -> list:
    lines: list = []
    words: list = []
    symbols: list = []
    isWord: bool = False

    tab: int = 9
    newLine: int = 10
    carriageReturn: int = 13
    space: int = 32

    for symbol in allBytes:
        if symbol is newLine:
            words.append(symbols)
            symbols = []
            lines.append(words)
            words = []
            isWord = False
        elif isWord is True and symbol is tab:
            words.append(symbols)
            symbols = list()
            isWord = False
        elif symbol is not space and symbol is not tab and symbol is not carriageReturn:
            isWord = True
            symbols.append(symbol)
    return lines


def __getMeasurements(allBytes: bytes, requiredSatelliteNumber: int, requiredSatelliteNumberSize: int,
                      requiredSatelliteNumber2: int = None) -> list:
    lines: list = __analyzeSyntaxAndReturnLinesList(allBytes)
    measurements: list = []
    numbersInLine: int = 21
    for line in lines:
        try:
            satelliteNumber: int = line[0][0]
            satelliteNumberSize: int = len(line[0])
            if requiredSatelliteNumber2 is not None:
                satelliteNumber2: int = line[0][1]
                if (satelliteNumber is requiredSatelliteNumber and
                        satelliteNumber2 is requiredSatelliteNumber2 and
                        satelliteNumberSize is requiredSatelliteNumberSize):
                    lineOfNumbers: list = []
                    for number in range(1, numbersInLine + 1):
                        numeric: float = __getNumeric(line, number)
                        lineOfNumbers.append(numeric)
                    measurements.append(lineOfNumbers)
            else:
                if (satelliteNumber is requiredSatelliteNumber and
                        satelliteNumberSize is requiredSatelliteNumberSize):
                    lineOfNumbers: list = []
                    for number in range(1, numbersInLine + 1):
                        numeric: float = __getNumeric(line, number)
                        lineOfNumbers.append(numeric)
                    measurements.append(lineOfNumbers)
        except Exception:
            pass
    return measurements


def __getNumeric(line: list, number: int) -> float:
    numberBuilder: str = ""
    numberLength: int = len(line[number])
    for digit in range(numberLength):
        symbol: chr = chr(line[number][digit])
        numberBuilder += symbol
    numeric: float = float(numberBuilder)
    return numeric


def __toAscii(number: str) -> int:
    return int(number) + 48


def getAngularVelocities(allBytes: bytes, amountOfObservations: int, satelliteNumberStr: str, satelliteNumberSize: int,
                         satelliteNumber2Str: str = None) -> list:
    satelliteNumber: int = __toAscii(satelliteNumberStr)
    angularVelocities: list = []
    if satelliteNumber2Str is not None:
        satelliteNumber2: int = __toAscii(satelliteNumber2Str)
        measurements: list = __getMeasurements(allBytes, satelliteNumber, satelliteNumberSize, satelliteNumber2)
    else:
        measurements: list = __getMeasurements(allBytes, satelliteNumber, satelliteNumberSize)

    oneHourInSeconds: float = 3600
    previousElevationAngle: float = measurements[0][14]
    for observation in range(amountOfObservations):
        currentElevationAngle: float = measurements[observation][14]
        angularVelocity: float = (currentElevationAngle - previousElevationAngle) / oneHourInSeconds
        angularVelocities.append(angularVelocity)
        previousElevationAngle = currentElevationAngle
    return angularVelocities


def __getAverageAngularVelocities(angularVelocities: list) -> list:
    averageVelocities: list = []
    observationsPerHour: int = 120

    firstHourSum: float = __getVelocitySumPerHour(angularVelocities, 0, 120)
    secondHourSum: float = __getVelocitySumPerHour(angularVelocities, 120, 240)
    thirdHourSum: float = __getVelocitySumPerHour(angularVelocities, 240, 360)

    firstHourAverage = firstHourSum / observationsPerHour
    secondHourAverage = secondHourSum / observationsPerHour
    thirdHourAverage = thirdHourSum / observationsPerHour

    averageVelocities.extend([firstHourAverage, secondHourAverage, thirdHourAverage])
    return averageVelocities


def getLinearVelocities(amountOfObservations: int) -> list:
    gravitational: float = 6.67 * pow(10, -11)
    earthMass: float = 5.972E24
    earthRadius: float = 6_371_000
    flightHeight: float = 20_000
    linearVelocities: list = []
    for observation in range(amountOfObservations):
        velocity: float = math.sqrt(gravitational * earthMass / (earthRadius + flightHeight))
        linearVelocities.append(velocity)
    return linearVelocities


def __getAverageLinearVelocities(amountOfObservations: int) -> list:
    averageVelocities: list = []
    linearVelocities: list = getLinearVelocities(amountOfObservations)
    observationsPerHour: int = 120

    firstHourSum: float = __getVelocitySumPerHour(linearVelocities, 0, 120)
    secondHourSum: float = __getVelocitySumPerHour(linearVelocities, 120, 240)
    thirdHourSum: float = __getVelocitySumPerHour(linearVelocities, 240, 360)

    firstHourAverage: float = firstHourSum / observationsPerHour
    secondHourAverage: float = secondHourSum / observationsPerHour
    thirdHourAverage: float = thirdHourSum / observationsPerHour
    averageVelocities.extend([firstHourAverage, secondHourAverage, thirdHourAverage])
    return averageVelocities


def __getVelocitySumPerHour(velocities: list, start: int, end: int) -> float:
    velocitySum: float = 0
    for observation in range(start, end):
        velocitySum += velocities[observation]
    return velocitySum


def __printAngularVelocities(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                             satellite1Velocities: list, satellite2Velocities: list, satellite3Velocities: list,
                             amountOfObservations: int) -> None:
    print("Угловая скорость\nСпутник #" + str(satellite1Number) + "\t\tСпутник #" + str(satellite2Number) +
          "\t\tСпутник #" + str(satellite3Number))
    for observation in range(amountOfObservations):
        satellite1Velocity: float = satellite1Velocities[observation]
        satellite2Velocity: float = satellite2Velocities[observation]
        satellite3Velocity: float = satellite3Velocities[observation]
        print(str(round(satellite1Velocity, 10)) + "\t" + str(round(satellite2Velocity, 10)) +
              "\t" + str(round(satellite3Velocity, 10)))
    print("***********************************************")


def __printAverageAngularVelocities(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                                    satellite1AngularVelocities: list, satellite2AngularVelocities: list,
                                    satellite3AngularVelocities: list) -> None:
    print("Средняя угловая скорость\nСпутник #" + str(satellite1Number) + "\t\tСпутник #" + str(satellite2Number) +
          "\t\tСпутник #" + str(satellite3Number))
    for hour in range(3):
        satellite1Velocity: float = __getAverageAngularVelocities(satellite1AngularVelocities)[hour]
        satellite2Velocity: float = __getAverageAngularVelocities(satellite2AngularVelocities)[hour]
        satellite3Velocity: float = __getAverageAngularVelocities(satellite3AngularVelocities)[hour]
        print(str(round(satellite1Velocity, 10)) + "\t" + str(round(satellite2Velocity, 10)) +
              "\t" + str(round(satellite3Velocity, 10)))
    print("***********************************************")


def __printLinearVelocities(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                            satellite1LinearVelocities: list, satellite2LinearVelocities: list,
                            satellite3LinearVelocities: list, amountOfObservations: int) -> None:
    print("Линейная скорость\nСпутник #" + str(satellite1Number) + "\t\tСпутник #" + str(satellite2Number) +
          "\t\tСпутник #" + str(satellite3Number))
    for observation in range(amountOfObservations):
        satellite1Velocity: float = satellite1LinearVelocities[observation]
        satellite2Velocity: float = satellite2LinearVelocities[observation]
        satellite3Velocity: float = satellite3LinearVelocities[observation]
        print(str(round(satellite1Velocity, 10)) + "\t" + str(round(satellite2Velocity, 10)) +
              "\t" + str(round(satellite3Velocity, 10)))
    print("***********************************************")


def __printAverageLinearVelocities(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                                   amountOfObservations: int) -> None:
    print("Средняя линейная скорость\nСпутник #" + str(satellite1Number) + "\t\tСпутник #" + str(satellite2Number) +
          "\t\tСпутник #" + str(satellite3Number))
    for hour in range(3):
        satellite1Velocity: float = __getAverageLinearVelocities(amountOfObservations)[hour]
        satellite2Velocity: float = __getAverageLinearVelocities(amountOfObservations)[hour]
        satellite3Velocity: float = __getAverageLinearVelocities(amountOfObservations)[hour]
        print(str(round(satellite1Velocity, 10)) + "\t" + str(round(satellite2Velocity, 10)) +
              "\t" + str(round(satellite3Velocity, 10)))
    print("***********************************************")


def printVelocities(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                    satellite1AngularVelocities: list, satellite2AngularVelocities: list,
                    satellite3AngularVelocities: list, satellite1LinearVelocities: list,
                    satellite2LinearVelocities: list, satellite3LinearVelocities: list,
                    amountOfObservations: int) -> None:
    __printAngularVelocities(satellite1Number, satellite2Number, satellite3Number, satellite1AngularVelocities,
                             satellite2AngularVelocities, satellite3AngularVelocities, amountOfObservations)
    __printAverageAngularVelocities(satellite1Number, satellite2Number, satellite3Number, satellite1AngularVelocities,
                                    satellite2AngularVelocities, satellite3AngularVelocities)
    __printLinearVelocities(satellite1Number, satellite2Number, satellite3Number, satellite1LinearVelocities,
                            satellite2LinearVelocities, satellite3LinearVelocities, amountOfObservations)
    __printAverageLinearVelocities(satellite1Number, satellite2Number, satellite3Number, amountOfObservations)


def __drawAngularVelocitiesGraph(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                                 satellite1AngularVelocities: list, satellite2AngularVelocities: list,
                                 satellite3AngularVelocities: list, amountOfObservations: int) -> None:
    observations = np.arange(1, amountOfObservations + 1)
    plt.plot(observations, satellite1AngularVelocities, 'o-', label="Спутник #" + str(satellite1Number))
    plt.plot(observations, satellite2AngularVelocities, 'o-', label="Спутник #" + str(satellite2Number))
    plt.plot(observations, satellite3AngularVelocities, 'o-', label="Спутник #" + str(satellite3Number))
    plt.xlabel("Время, секунды")
    plt.ylabel("Угловая скорость, рад/c")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def __drawAverageAngularVelocitiesGraph(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                                        satellite1AngularVelocities: list, satellite2AngularVelocities: list,
                                        satellite3AngularVelocities: list) -> None:
    satellite1Velocities: list = __getAverageAngularVelocities(satellite1AngularVelocities)
    satellite2Velocities: list = __getAverageAngularVelocities(satellite2AngularVelocities)
    satellite3Velocities: list = __getAverageAngularVelocities(satellite3AngularVelocities)
    observations = np.arange(1, 4)
    plt.plot(observations, satellite1Velocities, 'o-', label="Спутник #" + str(satellite1Number))
    plt.plot(observations, satellite2Velocities, 'o-', label="Спутник #" + str(satellite2Number))
    plt.plot(observations, satellite3Velocities, 'o-', label="Спутник #" + str(satellite3Number))
    plt.xlabel("Время, часы")
    plt.ylabel("Средняя угловая скорость, рад/c")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def __drawLinearVelocitiesGraph(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                                satellite1LinearVelocities: list, satellite2LinearVelocities: list,
                                satellite3LinearVelocities: list, amountOfObservations: int) -> None:
    observations = np.arange(1, amountOfObservations + 1)
    plt.plot(observations, satellite1LinearVelocities, 'o-', label="Спутник #" + str(satellite1Number))
    plt.plot(observations, satellite2LinearVelocities, 'o-', label="Спутник #" + str(satellite2Number))
    plt.plot(observations, satellite3LinearVelocities, 'o-', label="Спутник #" + str(satellite3Number))
    plt.xlabel("Время, секунды")
    plt.ylabel("Линейная скорость, м/с")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def __drawAverageLinearVelocitiesGraph(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                                       amountOfObservations: int) -> None:
    satellite1Velocities: list = __getAverageLinearVelocities(amountOfObservations)
    satellite2Velocities: list = __getAverageLinearVelocities(amountOfObservations)
    satellite3Velocities: list = __getAverageLinearVelocities(amountOfObservations)
    observations = np.arange(1, 4)
    plt.plot(observations, satellite1Velocities, 'o-', label="Спутник #" + str(satellite1Number))
    plt.plot(observations, satellite2Velocities, 'o-', label="Спутник #" + str(satellite2Number))
    plt.plot(observations, satellite3Velocities, 'o-', label="Спутник #" + str(satellite3Number))
    plt.xlabel("Время, часы")
    plt.ylabel("Средняя линейная скорость, рад/c")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def drawVelocities(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                   satellite1AngularVelocities: list, satellite2AngularVelocities: list,
                   satellite3AngularVelocities: list, satellite1LinearVelocities: list,
                   satellite2LinearVelocities: list, satellite3LinearVelocities: list,
                   amountOfObservations: int) -> None:
    __drawAngularVelocitiesGraph(satellite1Number, satellite2Number, satellite3Number, satellite1AngularVelocities,
                                 satellite2AngularVelocities, satellite3AngularVelocities, amountOfObservations)
    __drawAverageAngularVelocitiesGraph(satellite1Number, satellite2Number, satellite3Number,
                                        satellite1AngularVelocities, satellite2AngularVelocities,
                                        satellite3AngularVelocities)
    __drawLinearVelocitiesGraph(satellite1Number, satellite2Number, satellite3Number, satellite1LinearVelocities,
                                satellite2LinearVelocities, satellite3LinearVelocities, amountOfObservations)
    __drawAverageLinearVelocitiesGraph(satellite1Number, satellite2Number, satellite3Number, amountOfObservations)


def main():
    amountOfObservations: int = 360

    fileName: str = "resources/WTZZ_6hours.dat"
    allBytes: bytes = readBytes(fileName)

    satellite1AngularVelocities = getAngularVelocities(allBytes, amountOfObservations, "7", 1)
    satellite2AngularVelocities = getAngularVelocities(allBytes, amountOfObservations, "2", 2, "2")
    satellite3AngularVelocities = getAngularVelocities(allBytes, amountOfObservations, "2", 2, "3")

    satellite1LinearVelocities = getLinearVelocities(amountOfObservations)
    satellite2LinearVelocities = getLinearVelocities(amountOfObservations)
    satellite3LinearVelocities = getLinearVelocities(amountOfObservations)

    printVelocities(7, 22, 23, satellite1AngularVelocities, satellite2AngularVelocities, satellite3AngularVelocities,
                    satellite1LinearVelocities, satellite2LinearVelocities, satellite3LinearVelocities,
                    amountOfObservations)

    drawVelocities(7, 22, 23, satellite1AngularVelocities, satellite2AngularVelocities, satellite3AngularVelocities,
                   satellite1LinearVelocities, satellite2LinearVelocities, satellite3LinearVelocities,
                   amountOfObservations)


if __name__ == '__main__':
    main()
