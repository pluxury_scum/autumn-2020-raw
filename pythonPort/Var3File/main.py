import math
import matplotlib.pyplot as plt
import numpy as np


class UserGeo:
    halfCircle: float = 180
    latpp: float = 71.691643 / halfCircle
    lonpp: float = 128.865978 / halfCircle


class IGPGeo:
    halfCircle: float = 180
    lon1: float = 125 / halfCircle
    lon2: float = 130 / halfCircle
    lat1: float = 70 / halfCircle
    lat2: float = 72.5 / halfCircle


class AxisGeo:
    def __init__(self, lonpp: float, lon1: float, lon2: float, latpp: float, lat1: float, lat2: float) -> None:
        self.__xpp: float = (lonpp - lon1) / (lon2 - lon1)
        self.__ypp: float = (latpp - lat1) / (lat2 - lat1)

    @property
    def xpp(self) -> float:
        return self.__xpp

    @property
    def ypp(self) -> float:
        return self.__ypp


class WeightMatrix:
    def __init__(self, axisGeo: AxisGeo) -> None:
        self.__weights: list = [0] * 4
        xpp: float = axisGeo.xpp
        ypp: float = axisGeo.ypp
        self.__weights[0] = xpp * ypp
        self.__weights[1] = (1 - xpp) * ypp
        self.__weights[2] = (1 - xpp) * (1 - ypp)
        self.__weights[3] = xpp * (1 - ypp)

    def __getitem__(self, pos) -> float:
        return self.__weights[pos]


class IonCoefficients:
    def __init__(self, coefficients: list) -> None:
        self.__coefficients: list = coefficients

    def __getitem__(self, pos) -> float:
        return self.__coefficients[pos]


class Tec:
    def __init__(self, tec: list) -> None:
        self.__tec: list = tec

    def __getitem__(self, pos: int) -> float:
        return self.__tec[pos]


class GpsTime:
    def __init__(self, gpsTime: list) -> None:
        self.__gpsTime: list = gpsTime

    def __getitem__(self, pos: int) -> float:
        return self.__gpsTime[pos]


class IonosphericDelay:
    def __init__(self, weightMatrix: WeightMatrix, tec: Tec) -> None:
        self.__weightMatrix: WeightMatrix = weightMatrix
        self.__tec: Tec = tec

    @property
    def delayInMeters(self) -> float:
        tecuToMetersCoefficient: float = self.__tecuToMetersCoefficient
        delayInTecu: float = self.__delayInTecu
        delayInMeters: float = delayInTecu * tecuToMetersCoefficient
        return delayInMeters

    @property
    def __tecuToMetersCoefficient(self) -> float:
        l1: float = 1_575_420_000
        oneTecUnit: float = 1E16
        coefficient: float = 40.3 / pow(l1, 2) * oneTecUnit
        return coefficient

    @property
    def __delayInTecu(self) -> float:
        delay: float = 0
        for observation in range(4):
            weight: float = self.__weightMatrix[observation]
            rawTec: float = self.__tec[observation]
            tecInOneTecUnit: float = rawTec * 0.1
            delay += (weight * tecInOneTecUnit)
        return delay


class KlobucharModel:
    def __init__(self, gpsTime: float, alpha: IonCoefficients, beta: IonCoefficients):
        halfCircle: float = 180
        self.__gpsTime: float = gpsTime
        self.__elevationAngle: float = 90 / halfCircle
        self.__azimuth: float = 0
        self.__alpha: IonCoefficients = alpha
        self.__beta: IonCoefficients = beta

    @property
    def klobucharDelayInMeters(self) -> float:
        delayInSeconds: float = self.__klobucharDelayInSeconds
        speedOfLight: float = 2.99792458 * 1E8
        delayInMeters: float = delayInSeconds * speedOfLight
        return delayInMeters

    @property
    def __earthCenteredAngle(self) -> float:
        earthCenteredAngle: float = 0.0137 / (self.__elevationAngle + 0.11) - 0.022
        return earthCenteredAngle

    @property
    def __ippLatitude(self) -> float:
        latpp: float = UserGeo.latpp
        earthCenteredAngle: float = self.__earthCenteredAngle
        ippLatitude: float = latpp + earthCenteredAngle * math.cos(self.__azimuth)
        if ippLatitude > 0.416:
            ippLatitude = 0.416
        elif ippLatitude < -0.416:
            ippLatitude = -0.416
        return ippLatitude

    @property
    def __ippLongtitude(self) -> float:
        lonpp: float = UserGeo.lonpp
        earthCenteredAngle: float = self.__earthCenteredAngle
        ippLatitude: float = self.__ippLatitude
        ippLongtitude: float = lonpp + (earthCenteredAngle * math.sin(self.__azimuth) / (math.cos(ippLatitude)))
        return ippLongtitude

    @property
    def __ippGeomagneticLatitude(self) -> float:
        ippLatitude: float = self.__ippLatitude
        ippLongtitude: float = self.__ippLongtitude
        ippGeomagneticLatitude: float = ippLatitude + 0.064 * math.cos(ippLongtitude - 1.617)
        return ippGeomagneticLatitude

    @property
    def __ippLocalTime(self) -> float:
        secondsInOneDay: float = 86_400
        secondsInTwelveHours: float = 43_200
        ippLongtitude: float = self.__ippLongtitude
        ippLocalTime: float = secondsInTwelveHours * ippLongtitude + self.__gpsTime
        while ippLocalTime > secondsInOneDay:
            ippLocalTime -= secondsInOneDay
        while ippLocalTime < 0:
            ippLocalTime += secondsInOneDay
        return ippLocalTime

    @property
    def __ionosphericDelayAmplitude(self) -> float:
        ippGeomagneticLatitude: float = self.__ippGeomagneticLatitude
        amplitude: float = 0
        for i in range(4):
            amplitude += (self.__alpha[i] * pow(ippGeomagneticLatitude, i))
        if amplitude < 0:
            amplitude = 0
        return amplitude

    @property
    def __ionosphericDelayPeriod(self) -> float:
        ippGeomagneticLatitude: float = self.__ippGeomagneticLatitude
        period: float = 0
        for i in range(4):
            period += (self.__beta[i] * pow(ippGeomagneticLatitude, i))
        if period < 72_000:
            period = 72_000
        return period

    @property
    def __ionosphericDelayPhase(self) -> float:
        ippLocalTime: float = self.__ippLocalTime
        ionosphericDelayPeriod: float = self.__ionosphericDelayPeriod
        ionosphericDelayPhase: float = 2 * math.pi * (ippLocalTime - 50_400) / ionosphericDelayPeriod
        return ionosphericDelayPhase

    @property
    def __slantFactor(self) -> float:
        slantFactor: float = 1.0 + 16.0 * math.pow((0.53 - self.__elevationAngle), 3)
        return slantFactor

    @property
    def __klobucharDelayInSeconds(self) -> float:
        ionosphericDelayPhase: float = self.__ionosphericDelayPhase
        ionosphericDelayAmplitude: float = self.__ionosphericDelayAmplitude
        slantFactor: float = self.__slantFactor
        ionosphericTimeDelay: float = 0
        if math.fabs(ionosphericDelayPhase) > 1.57:
            ionosphericTimeDelay = 5E-9 * slantFactor
        else:
            ionosphericTimeDelay = (5E-9 + ionosphericDelayAmplitude * (1 - math.pow(ionosphericDelayPhase, 2) / 2 +
                                                                        math.pow(ionosphericDelayPhase, 4) / 24)) * slantFactor
        return ionosphericTimeDelay


class IonosphericDelaysFactory:
    __lonFirst: int = -180
    __dlon: int = 5
    __tecValuesPerLine: int = 16

    def __init__(self, weightMatrix: WeightMatrix, lon1: int, lon2: int, lon3: int, lon4: int,
                 amountOfObservations: int):
        self.__amountOfObservations: int = amountOfObservations
        self.__weightMatrix: WeightMatrix = weightMatrix
        self.__lon1: int = lon1
        self.__lon2: int = lon2
        self.__lon3: int = lon3
        self.__lon4: int = lon4

    def createDelays(self, tecA1: list, tecA2: list, tecA3: list, tecA4: list) -> list:
        delays: list = []
        self.__setRows()
        self.__setPos()

        for observation in range(self.__amountOfObservations):
            a1: int = tecA1[observation][self.__rowA1][self.__posA1]
            a2: int = tecA2[observation][self.__rowA2][self.__posA2]
            a3: int = tecA3[observation][self.__rowA3][self.__posA3]
            a4: int = tecA4[observation][self.__rowA4][self.__posA4]

            tecArray: list = []
            tecArray.extend([a1, a2, a3, a4])

            tec: Tec = Tec(tecArray)
            delay: IonosphericDelay = IonosphericDelay(self.__weightMatrix, tec)
            delays.append(delay)
        return delays

    def __setPos(self) -> None:
        self.__posA1 = self.__getPos(self.__rowA1, self.__lon1)
        self.__posA2 = self.__getPos(self.__rowA2, self.__lon2)
        self.__posA3 = self.__getPos(self.__rowA3, self.__lon3)
        self.__posA4 = self.__getPos(self.__rowA4, self.__lon4)

    def __getPos(self, row: int, lon: int) -> int:
        number: int = int(abs(((self.__lonFirst - lon) / self.__dlon)) - (row * self.__tecValuesPerLine))
        return number

    def __setRows(self) -> None:
        self.__rowA1 = self.__getRow(self.__lon1)
        self.__rowA2 = self.__getRow(self.__lon2)
        self.__rowA3 = self.__getRow(self.__lon3)
        self.__rowA4 = self.__getRow(self.__lon4)

    def __getRow(self, lon: int) -> int:
        row: int = int(abs((self.__lonFirst - lon) / (self.__tecValuesPerLine * self.__dlon)))
        return row


class KlobucharDelaysFactory:
    def __init__(self, gpsTime: GpsTime, alpha: IonCoefficients, beta: IonCoefficients,
                 amountOfObservations: int) -> None:
        self.__gpsTime: GpsTime = gpsTime
        self.__alpha: IonCoefficients = alpha
        self.__beta: IonCoefficients = beta
        self.__amountOfObservations: int = amountOfObservations

    def createKlobuchar(self) -> list:
        models: list = []
        for observation in range(self.__amountOfObservations):
            time: float = self.__gpsTime[observation]
            model: KlobucharModel = KlobucharModel(time, self.__alpha, self.__beta)
            models.append(model)
        return models


class EphemerisFileReader:
    def __init__(self, amountOfObservations: int, fileName: str) -> None:
        self.__amountOfObservations: int = amountOfObservations
        with open(fileName, "rb") as file:
            self.__allBytes: bytes = file.read()

    @property
    def alpha(self) -> list:
        alpha: list = self.__extractIonCoeffs(3)
        return alpha

    @property
    def beta(self) -> list:
        beta: list = self.__extractIonCoeffs(4)
        return beta

    def __extractIonCoeffs(self, lineNumber: int) -> list:
        lines: list = self.__analyzeSyntaxAndReturnLinesList()
        line: list = lines[lineNumber]
        coeffs: list = []
        for coefficient in range(4):
            numeric: float = self.__getCoeffNumeric(line, coefficient)
            coeffs.append(numeric)
        return coeffs

    @classmethod
    def __getCoeffNumeric(cls, line: list, number: int) -> float:
        numberBuilder: str = ""
        digits: int = len(line[number])
        for digit in range(digits):
            symbol: chr = chr(line[number][digit])
            if symbol is 'D':
                numberBuilder += 'E'
            else:
                numberBuilder += symbol
        numeric: float = float(numberBuilder)
        return numeric

    def getGpsTime(self, requiredSatelliteNumberChar: str, requiredSatelliteNumberSize: int,
                   requiredSatelliteNumber2Char: str = None) -> list:
        lines: list = self.__analyzeSyntaxAndReturnLinesList()
        gpsTime: list = [0] * self.__amountOfObservations
        startOfObservations: int = 8
        linesPerObservations: int = 8
        asciiStarts: int = 48

        for observation in range(startOfObservations, len(lines), linesPerObservations):
            try:
                minutesFirstNumber: int = lines[observation][5][0] - asciiStarts
                minutesNumberSize: int = len(lines[observation][5])
                if minutesFirstNumber is 0 and minutesNumberSize is 1:
                    if requiredSatelliteNumber2Char is not None:
                        requiredSatelliteNumber: int = self.__toAscii(requiredSatelliteNumberChar)
                        requiredSatelliteNumber2: int = self.__toAscii(requiredSatelliteNumber2Char)
                        satelliteNumber: int = lines[observation][0][0]
                        satelliteNumber2: int = lines[observation][0][1]
                        satelliteNumberSize = len(lines[observation][0])
                        hourFirstNumber: int = lines[observation][4][0] - asciiStarts
                        if (len(lines[observation][4]) is 1 and
                                satelliteNumber is requiredSatelliteNumber and
                                satelliteNumber2 is requiredSatelliteNumber2 and
                                satelliteNumberSize is requiredSatelliteNumberSize):
                            numeric: float = self.__getGpsTimeNumeric(lines, observation)
                            gpsTime[int(hourFirstNumber / 2)] = numeric
                        else:
                            hourSecondNumber: int = lines[observation][4][1] - asciiStarts
                            hourFull: str = str(hourFirstNumber) + str(hourSecondNumber)
                            hourValue: int = int(hourFull)

                            if (satelliteNumber is requiredSatelliteNumber and
                                    satelliteNumber2 is requiredSatelliteNumber2 and
                                    satelliteNumberSize is requiredSatelliteNumberSize):
                                numeric: float = self.__getGpsTimeNumeric(lines, observation)
                                gpsTime[int(hourValue / 2)] = numeric
                    else:
                        requiredSatelliteNumber: int = self.__toAscii(requiredSatelliteNumberChar)
                        satelliteNumber: int = lines[observation][0][0]
                        satelliteNumberSize = len(lines[observation][0])
                        hourFirstNumber: int = lines[observation][4][0] - asciiStarts
                        if (len(lines[observation][4]) is 1 and
                                satelliteNumber is requiredSatelliteNumber and
                                satelliteNumberSize is requiredSatelliteNumberSize):
                            numeric: float = self.__getGpsTimeNumeric(lines, observation)
                            gpsTime[int(hourFirstNumber / 2)] = numeric
                        else:
                            hourSecondNumber: int = lines[observation][4][1] - asciiStarts
                            hourFull: str = str(hourFirstNumber) + str(hourSecondNumber)
                            hourValue: int = int(hourFull)

                            if (satelliteNumber is requiredSatelliteNumber and
                                    satelliteNumberSize is requiredSatelliteNumberSize):
                                numeric: float = self.__getGpsTimeNumeric(lines, observation)
                                gpsTime[int(hourValue / 2)] = numeric
            except Exception:
                pass
        return gpsTime

    def __toAscii(self, number: str) -> int:
        return int(number) + 48

    @classmethod
    def __getGpsTimeNumeric(cls, lines: list, observation: int) -> float:
        numberBuilder: str = ""
        observationOffset: int = 7
        digits: int = len(lines[observation + observationOffset][0])
        for digit in range(digits):
            symbol: chr = chr(lines[observation + observationOffset][0][digit])
            if symbol is 'D':
                numberBuilder += 'E'
            else:
                numberBuilder += symbol
        numeric: float = float(numberBuilder)
        return numeric

    def __analyzeSyntaxAndReturnLinesList(self) -> list:
        lines: list = []
        words: list = []
        symbols: list = []
        isWord: bool = False

        newLine: int = 10
        space: int = 32

        for symbol in self.__allBytes:
            if symbol is newLine:
                words.append(symbols)
                symbols = []
                lines.append(words)
                words = []
                isWord = False
            elif isWord is True and symbol is space:
                words.append(symbols)
                symbols = []
                isWord = False
            elif symbol is not space:
                isWord = True
                symbols.append(symbol)
        return lines


class IonoFileReader:
    def __init__(self, fileName: str) -> None:
        with open(fileName, "rb") as file:
            self.__allBytes: bytes = file.read()

    def getTecArray(self, requiredFirstLatDigitChar: str, requiredSecondLatDigitChar: str, requiredThirdLatDigitChar: str,
                    firstLine: int) -> list:
        requiredFirstLatDigit: int = 0
        requiredSecondLatDigit: int = self.__toAscii(requiredSecondLatDigitChar)
        requiredThirdLatDigit: int = 0
        if requiredFirstLatDigitChar is "-":
            requiredFirstLatDigit = 45
        else:
            requiredFirstLatDigit = self.__toAscii(requiredFirstLatDigitChar)

        if requiredThirdLatDigitChar is ".":
            requiredThirdLatDigit = 46
        else:
            requiredThirdLatDigit = self.__toAscii(requiredThirdLatDigitChar)

        lines: list = self.__analyzeSyntaxAndReturnLinesList()
        tecArray: list = []
        amountOfLinesWithTec: int = firstLine + 5575
        for line in range(firstLine, amountOfLinesWithTec):
            try:
                firstDigitOfLat: int = lines[line][0][0]
                secondDigitOfLat: int = lines[line][0][1]
                thirdDigitOfLat: int = lines[line][0][2]
                linesWithTecPerLat: int = 5
                if (firstDigitOfLat is requiredFirstLatDigit and
                        secondDigitOfLat is requiredSecondLatDigit and
                        thirdDigitOfLat is requiredThirdLatDigit):
                    tecPerLat: list = []
                    for lineWithTec in range(1, linesWithTecPerLat + 1):
                        numbersLine: list = self.__getNumberLine(lines, line, lineWithTec)
                        tecPerLat.append(numbersLine)
                    tecArray.append(tecPerLat)
            except Exception:
                pass
        return tecArray

    @classmethod
    def __toAscii(cls, number: str) -> int:
        return int(number) + 48

    def __getNumberLine(self, lines: list, line: int, lineWithTec: int) -> list:
        numbersLine: list = []
        numbersInRow: int = len(lines[line + lineWithTec])
        for number in range(numbersInRow):
            numeric: int = self.__getNumeric(lines, line, lineWithTec, number)
            numbersLine.append(numeric)
        return numbersLine

    @classmethod
    def __getNumeric(cls, lines: list, line: int, lineWithTec: int, number: int) -> int:
        numberBuilder: str = ""
        numberLength: int = len(lines[line + lineWithTec][number])
        for digit in range(numberLength):
            symbol: chr = chr(lines[line + lineWithTec][number][digit])
            numberBuilder += symbol
        numeric: int = int(numberBuilder)
        return numeric

    def __analyzeSyntaxAndReturnLinesList(self) -> list:
        lines: list = []
        words: list = []
        symbols: list = []
        isWord: bool = False

        newLine: int = 10
        space: int = 32

        for symbol in self.__allBytes:
            if symbol is newLine:
                words.append(symbols)
                symbols = []
                lines.append(words)
                words = []
                isWord = False
            elif isWord is True and symbol is space:
                words.append(symbols)
                symbols = []
                isWord = False
            elif symbol is not space:
                isWord = True
                symbols.append(symbol)
        return lines


class ConsoleOutput:
    def __init__(self, forecastDelays: list, preciseDelays: list, klobucharDelays: list,
                 amountOfObservations: int) -> None:
        self.__forecastDelays: list = forecastDelays
        self.__preciseDelays: list = preciseDelays
        self.__klobucharDelays: list = klobucharDelays
        self.__amountOfObservations: int = amountOfObservations

    def printDelays(self) -> None:
        print("igrg\tigsg\tklobuchar")
        for observation in range(self.__amountOfObservations):
            forecastValue: float = self.__forecastDelays[observation].delayInMeters
            preciseValue: float = self.__preciseDelays[observation].delayInMeters
            klobucharValue: float = self.__klobucharDelays[observation].klobucharDelayInMeters
            print(str(round(forecastValue, 3)) + "\t" + str(round(preciseValue, 3)) + "\t" +
                  str(round(klobucharValue, 3)))


class GraphDrawer:
    def __init__(self, forecastDelays: list, preciseDelays: list, klobucharDelays: list,
                 amountOfObservations: int) -> None:
        self.__forecastDelays: list = forecastDelays
        self.__preciseDelays: list = preciseDelays
        self.__klobucharDelays: list = klobucharDelays
        self.__amountOfObservations: int = amountOfObservations

    def showDelays(self) -> None:
        forecastValues: list = []
        preciseValues: list = []
        klobucharValues: list = []
        observations = np.arange(0, self.__amountOfObservations)
        for observation in range(self.__amountOfObservations):
            forecastValue: float = self.__forecastDelays[observation].delayInMeters
            preciseValue: float = self.__preciseDelays[observation].delayInMeters
            klobucharValue: float = self.__klobucharDelays[observation].klobucharDelayInMeters
            forecastValues.append(forecastValue)
            preciseValues.append(preciseValue)
            klobucharValues.append(klobucharValue)
        plt.plot(observations * 2, forecastValues, 'o-', label="igrg")
        plt.plot(observations * 2, preciseValues, 'o-', label="igsg")
        plt.plot(observations * 2, klobucharValues, 'o-', label="Klobuchar")
        plt.locator_params(axis='x', nbins=24)
        plt.xlabel("Время, час")
        plt.ylabel("Ионосферная поправка, метр")
        plt.legend()
        plt.grid(linestyle='-', linewidth=0.5)
        plt.show()


def main():
    amountOfObservations: int = 12

    axisGeo: AxisGeo = AxisGeo(UserGeo.lonpp, IGPGeo.lon1, IGPGeo.lon2, UserGeo.latpp, IGPGeo.lat1, IGPGeo.lat2)
    weightMatrix: WeightMatrix = WeightMatrix(axisGeo)

    fileNameEphemeris: str = "resources/brdc0010.18n"
    fileReaderEphemeris: EphemerisFileReader = EphemerisFileReader(amountOfObservations, fileNameEphemeris)
    alpha: IonCoefficients = IonCoefficients(fileReaderEphemeris.alpha)
    beta: IonCoefficients = IonCoefficients(fileReaderEphemeris.beta)
    gpsTime: GpsTime = GpsTime(fileReaderEphemeris.getGpsTime("1", 1))

    fileNameForecast: str = "resources/igrg0010.18i"
    ionoFileReaderForecast: IonoFileReader = IonoFileReader(fileNameForecast)

    forecastA1: list = ionoFileReaderForecast.getTecArray("7", "2", ".", 304)
    forecastA2: list = ionoFileReaderForecast.getTecArray("7", "2", ".", 304)
    forecastA3: list = ionoFileReaderForecast.getTecArray("7", "0", ".", 304)
    forecastA4: list = ionoFileReaderForecast.getTecArray("7", "0", ".", 304)

    fileNamePrecise: str = "resources/igsg0010.18i"
    ionoFileReaderPrecise: IonoFileReader = IonoFileReader(fileNamePrecise)

    preciseA1: list = ionoFileReaderPrecise.getTecArray("7", "2", ".", 394)
    preciseA2: list = ionoFileReaderPrecise.getTecArray("7", "2", ".", 394)
    preciseA3: list = ionoFileReaderPrecise.getTecArray("7", "0", ".", 394)
    preciseA4: list = ionoFileReaderPrecise.getTecArray("7", "0", ".", 394)

    ionosphericDelaysFactory: IonosphericDelaysFactory = IonosphericDelaysFactory(weightMatrix, 130, 125, 125, 130,
                                                                                  amountOfObservations)
    klobucharDelaysFactory: KlobucharDelaysFactory = KlobucharDelaysFactory(gpsTime, alpha, beta, amountOfObservations)

    forecastDelays: list = ionosphericDelaysFactory.createDelays(forecastA1, forecastA2, forecastA3, forecastA4)
    preciseDelays: list = ionosphericDelaysFactory.createDelays(preciseA1, preciseA2, preciseA3, preciseA4)
    klobucharDelays: list = klobucharDelaysFactory.createKlobuchar()

    consoleOutput: ConsoleOutput = ConsoleOutput(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations)
    consoleOutput.printDelays()

    graphDrawer: GraphDrawer = GraphDrawer(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations)
    graphDrawer.showDelays()


if __name__ == "__main__":
    main()
