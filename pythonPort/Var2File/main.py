from abc import ABC, abstractmethod
import math
import matplotlib.pyplot as plt
import numpy as np


class ElevationAngles:
    def __init__(self, elevationAngles: list) -> None:
        self.__elevationAngles = elevationAngles

    def __getitem__(self, observation: int) -> float:
        return self.__elevationAngles[observation]


class AngularVelocities:
    def __init__(self, elevationAngles: ElevationAngles, amountOfObservations: int) -> None:
        self.__amountOfObservations: int = amountOfObservations
        self.__elevationAngles: ElevationAngles = elevationAngles
        self.__angularVelocities: list = []

    @property
    def averageAngularVelocities(self) -> list:
        averageVelocities: list = []
        angularVelocities: list = self.angularVelocities
        observationsPerHour: int = 120

        firstHourSum: float = self.__getVelocitySumPerHour(angularVelocities, 0, 120)
        secondHourSum: float = self.__getVelocitySumPerHour(angularVelocities, 120, 240)
        thirdHourSum: float = self.__getVelocitySumPerHour(angularVelocities, 240, 360)

        firstHourAverage: float = firstHourSum / observationsPerHour
        secondHourAverage: float = secondHourSum / observationsPerHour
        thirdHourAverage: float = thirdHourSum / observationsPerHour
        averageVelocities.extend([firstHourAverage, secondHourAverage, thirdHourAverage])
        return averageVelocities

    @property
    def angularVelocities(self) -> list:
        oneHourInSeconds: float = 3600
        previousElevationAngle: float = self.__elevationAngles[0]
        for observation in range(self.__amountOfObservations):
            currentElevationAngle: float = self.__elevationAngles[observation]
            angularVelocity: float = (currentElevationAngle - previousElevationAngle) / oneHourInSeconds
            self.__angularVelocities.append(angularVelocity)
            previousElevationAngle = currentElevationAngle
        return self.__angularVelocities

    @classmethod
    def __getVelocitySumPerHour(cls, velocities: list, start: int, end: int) -> float:
        hourSum: float = 0
        for observation in range(start, end):
            hourSum += velocities[observation]
        return hourSum


class LinearVelocities:
    def __init__(self, amountOfObservations: int) -> None:
        self.__amountOfObservations: int = amountOfObservations
        self.__linearVelocities: list = []

    @property
    def averageLinearVelocities(self) -> list:
        averageVelocities: list = []
        linearVelocities: list = self.linearVelocities
        observationsPerHour: int = 120

        firstHourSum: float = self.__getVelocitySumPerHour(linearVelocities, 0, 120)
        secondHourSum: float = self.__getVelocitySumPerHour(linearVelocities, 120, 240)
        thirdHourSum: float = self.__getVelocitySumPerHour(linearVelocities, 240, 360)

        firstHourAverage: float = firstHourSum / observationsPerHour
        secondHourAverage: float = secondHourSum / observationsPerHour
        thirdHourAverage: float = thirdHourSum / observationsPerHour
        averageVelocities.extend([firstHourAverage, secondHourAverage, thirdHourAverage])
        return averageVelocities

    @property
    def linearVelocities(self) -> list:
        gravitational: float = 6.67 * pow(10, -11)
        earthMass: float = 5.972E24
        earthRadius: float = 6_371_000
        flightHeight: float = 20_000
        for observation in range(self.__amountOfObservations):
            velocity: float = math.sqrt(gravitational * earthMass / (earthRadius + flightHeight))
            self.__linearVelocities.append(velocity)
        return self.__linearVelocities

    @classmethod
    def __getVelocitySumPerHour(cls, velocities: list, start: int, end: int) -> float:
        velocitySum: float = 0
        for observation in range(start, end):
            velocitySum += velocities[observation]
        return velocitySum


class Satellite:
    def __init__(self, number: int, angularVelocities: AngularVelocities, linearVelocities: LinearVelocities) -> None:
        self.__number: int = number
        self.__angularVelocities: AngularVelocities = angularVelocities
        self.__linearVelocities: LinearVelocities = linearVelocities

    @property
    def number(self) -> int:
        return self.number

    @property
    def angularVelocities(self) -> list:
        velocities: list = self.__angularVelocities.angularVelocities
        return velocities

    @property
    def averageAngularVelocities(self) -> list:
        velocities: list = self.__angularVelocities.averageAngularVelocities
        return velocities

    @property
    def linearVelocities(self) -> list:
        velocities: list = self.__linearVelocities.linearVelocities
        return velocities

    @property
    def averageLinearVelocities(self) -> list:
        velocities: list = self.__linearVelocities.averageLinearVelocities
        return velocities


class FileReader:
    def __init__(self, fileName: str) -> None:
        with open(fileName, "rb") as file:
            self.__allBytes: bytes = file.read()

    def getMeasurements(self, requiredSatelliteNumber: int, requiredSatelliteNumberSize: int,
                        requiredSatelliteNumber2: int = None) -> list:
        lines: list = self.__analyzeSyntaxAndReturnLinesList()
        measurements: list = []
        numbersInLine: int = 21
        for line in lines:
            try:
                satelliteNumber: int = line[0][0]
                satelliteNumberSize: int = len(line[0])
                if requiredSatelliteNumber2 is not None:
                    satelliteNumber2: int = line[0][1]
                    if (satelliteNumber is requiredSatelliteNumber and
                            satelliteNumber2 is requiredSatelliteNumber2 and
                            satelliteNumberSize is requiredSatelliteNumberSize):
                        lineOfNumbers: list = []
                        for number in range(1, numbersInLine + 1):
                            numeric: float = self.__getNumeric(line, number)
                            lineOfNumbers.append(numeric)
                        measurements.append(lineOfNumbers)
                else:
                    if (satelliteNumber is requiredSatelliteNumber and
                            satelliteNumberSize is requiredSatelliteNumberSize):
                        lineOfNumbers: list = []
                        for number in range(1, numbersInLine + 1):
                            numeric: float = self.__getNumeric(line, number)
                            lineOfNumbers.append(numeric)
                        measurements.append(lineOfNumbers)
            except Exception:
                pass
        return measurements

    @classmethod
    def __getNumeric(cls, line: list, number: int) -> float:
        numberBuilder: str = ""
        numberLength: int = len(line[number])
        for digit in range(numberLength):
            symbol: chr = chr(line[number][digit])
            numberBuilder += symbol
        numeric: float = float(numberBuilder)
        return numeric

    def __analyzeSyntaxAndReturnLinesList(self) -> list:
        lines: list = []
        words: list = []
        symbols: list = []
        isWord: bool = False

        tab: int = 9
        newLine: int = 10
        carriageReturn: int = 13
        space: int = 32

        for symbol in self.__allBytes:
            if symbol is newLine:
                words.append(symbols)
                symbols = []
                lines.append(words)
                words = []
                isWord = False
            elif isWord is True and symbol is tab:
                words.append(symbols)
                symbols = []
                isWord = False
            elif symbol is not space and symbol is not tab and symbol is not carriageReturn:
                isWord = True
                symbols.append(symbol)
        return lines


class SatelliteFactory:
    def __init__(self, fileReader: FileReader, amountOfObservations: int) -> None:
        self.__amountOfObservations: int = amountOfObservations
        self.__fileReader: FileReader = fileReader

    def createSatellite(self, satelliteNumber: str, satelliteNumberSize: int,
                        satelliteNumber2: str = None) -> Satellite:
        satelliteNumberInAscii: int = self.__toAscii(satelliteNumber)
        elevationAnglesArray: list = []
        if satelliteNumber2 is not None:
            satelliteNumberFull: str = satelliteNumber + satelliteNumber2
            satelliteNumberNumeric = int(satelliteNumberFull)
            satelliteNumber2InAscii: int = self.__toAscii(satelliteNumber2)
            measurements: list = self.__fileReader.getMeasurements(satelliteNumberInAscii, satelliteNumberSize,
                                                                   satelliteNumber2InAscii)
            for observation in range(self.__amountOfObservations):
                elevationAngle = measurements[observation][14]
                elevationAnglesArray.append(elevationAngle)
        else:
            satelliteNumberNumeric = int(satelliteNumber)
            measurements: list = self.__fileReader.getMeasurements(satelliteNumberInAscii, satelliteNumberSize)
            for observation in range(self.__amountOfObservations):
                elevationAngle = measurements[observation][14]
                elevationAnglesArray.append(elevationAngle)

        elevationAngles: ElevationAngles = ElevationAngles(elevationAnglesArray)
        angularVelocities: AngularVelocities = AngularVelocities(elevationAngles, self.__amountOfObservations)
        linearVelocities: LinearVelocities = LinearVelocities(self.__amountOfObservations)
        satellite: Satellite = Satellite(satelliteNumberNumeric, angularVelocities, linearVelocities)
        return satellite

    @classmethod
    def __toAscii(cls, number: str) -> int:
        return int(number) + 48


class ConsoleOutput:
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite, amountOfObservations: int,
                 amountOfHours: int) -> None:
        self.__satellite1: Satellite = satellite1
        self.__satellite2: Satellite = satellite2
        self.__satellite3: Satellite = satellite3
        self.__amountOfObservations = amountOfObservations
        self.__amountOfHours = amountOfHours

    def printVelocities(self) -> None:
        angularVelocitiesOutput = AngularVelocitiesConsoleOutput(self.__satellite1, self.__satellite2,
                                                                 self.__satellite3, self.__amountOfObservations)
        angularVelocitiesOutput.printOutput()

        averageAngularVelocitiesOutput = AverageAngularVelocitiesConsoleOutput(self.__satellite1, self.__satellite2,
                                                                               self.__satellite3, self.__amountOfHours)
        averageAngularVelocitiesOutput.printOutput()

        linearVelocitiesOutput = LinearVelocitiesConsoleOutput(self.__satellite1, self.__satellite2, self.__satellite3,
                                                               self.__amountOfObservations)
        linearVelocitiesOutput.printOutput()

        averageLinearVelocitiesOutput = AverageLinearVelocitiesConsoleOutput(self.__satellite1, self.__satellite2,
                                                                             self.__satellite3, self.__amountOfHours)
        averageLinearVelocitiesOutput.printOutput()


class TemplateConsoleOutput(ABC):
    def __init__(self, amountOfObservations: int) -> None:
        self.__amountOfObservations = amountOfObservations

    def printOutput(self) -> None:
        number1: int = self.getSatellite1Number()
        number2: int = self.getSatellite2Number()
        number3: int = self.getSatellite3Number()
        legend: str = self.getLegend()
        print(legend)
        print("Спутник #" + str(number1) + "\tСпутник #" + str(number2) + "\tСпутник #" + str(number3))
        for observation in range(self.__amountOfObservations):
            satellite1Velocity: float = self.getVelocities1()[observation]
            satellite2Velocity: float = self.getVelocities2()[observation]
            satellite3Velocity: float = self.getVelocities3()[observation]
            print(str(round(satellite1Velocity, 10)) + "\t" + str(round(satellite2Velocity, 10)) +
                  "\t" + str(round(satellite3Velocity, 10)))
        print("***********************************************")

    @abstractmethod
    def getLegend(self) -> str:
        pass

    @abstractmethod
    def getSatellite1Number(self) -> int:
        pass

    @abstractmethod
    def getSatellite2Number(self) -> int:
        pass

    @abstractmethod
    def getSatellite3Number(self) -> int:
        pass

    @abstractmethod
    def getVelocities1(self) -> list:
        pass

    @abstractmethod
    def getVelocities2(self) -> list:
        pass

    @abstractmethod
    def getVelocities3(self) -> list:
        pass


class AngularVelocitiesConsoleOutput(TemplateConsoleOutput):
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        super().__init__(amountOfObservations)
        self.__satellite1 = satellite1
        self.__satellite2 = satellite2
        self.__satellite3 = satellite3

    def getLegend(self) -> str:
        legend: str = "Угловая скорость"
        return legend

    def getSatellite1Number(self) -> int:
        number: int = self.__satellite1.number
        return number

    def getSatellite2Number(self) -> int:
        number: int = self.__satellite2.number
        return number

    def getSatellite3Number(self) -> int:
        number: int = self.__satellite3.number
        return number

    def getVelocities1(self) -> list:
        velocities: list = self.__satellite1.angularVelocities
        return velocities

    def getVelocities2(self) -> list:
        velocities: list = self.__satellite2.angularVelocities
        return velocities

    def getVelocities3(self) -> list:
        velocities: list = self.__satellite3.angularVelocities
        return velocities


class AverageAngularVelocitiesConsoleOutput(TemplateConsoleOutput):
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        super().__init__(amountOfObservations)
        self.__satellite1 = satellite1
        self.__satellite2 = satellite2
        self.__satellite3 = satellite3

    def getLegend(self) -> str:
        legend: str = "Средняя угловая скорость"
        return legend

    def getSatellite1Number(self) -> int:
        number: int = self.__satellite1.number
        return number

    def getSatellite2Number(self) -> int:
        number: int = self.__satellite2.number
        return number

    def getSatellite3Number(self) -> int:
        number: int = self.__satellite3.number
        return number

    def getVelocities1(self) -> list:
        velocities: list = self.__satellite1.averageAngularVelocities
        return velocities

    def getVelocities2(self) -> list:
        velocities: list = self.__satellite2.averageAngularVelocities
        return velocities

    def getVelocities3(self) -> list:
        velocities: list = self.__satellite3.averageAngularVelocities
        return velocities


class LinearVelocitiesConsoleOutput(TemplateConsoleOutput):
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        super().__init__(amountOfObservations)
        self.__satellite1 = satellite1
        self.__satellite2 = satellite2
        self.__satellite3 = satellite3

    def getLegend(self) -> str:
        legend: str = "Линейная скорость"
        return legend

    def getSatellite1Number(self) -> int:
        number: int = self.__satellite1.number
        return number

    def getSatellite2Number(self) -> int:
        number: int = self.__satellite2.number
        return number

    def getSatellite3Number(self) -> int:
        number: int = self.__satellite3.number
        return number

    def getVelocities1(self) -> list:
        velocities: list = self.__satellite1.linearVelocities
        return velocities

    def getVelocities2(self) -> list:
        velocities: list = self.__satellite2.linearVelocities
        return velocities

    def getVelocities3(self) -> list:
        velocities: list = self.__satellite3.linearVelocities
        return velocities


class AverageLinearVelocitiesConsoleOutput(TemplateConsoleOutput):
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        super().__init__(amountOfObservations)
        self.__satellite1 = satellite1
        self.__satellite2 = satellite2
        self.__satellite3 = satellite3

    def getLegend(self) -> str:
        legend: str = "Средняя линейная скорость"
        return legend

    def getSatellite1Number(self) -> int:
        number: int = self.__satellite1.number
        return number

    def getSatellite2Number(self) -> int:
        number: int = self.__satellite2.number
        return number

    def getSatellite3Number(self) -> int:
        number: int = self.__satellite3.number
        return number

    def getVelocities1(self) -> list:
        velocities: list = self.__satellite1.averageLinearVelocities
        return velocities

    def getVelocities2(self) -> list:
        velocities: list = self.__satellite2.averageLinearVelocities
        return velocities

    def getVelocities3(self) -> list:
        velocities: list = self.__satellite3.averageLinearVelocities
        return velocities


class GraphDrawer:
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite, amountOfObservations: int,
                 amountOfHours: int) -> None:
        self.__satellite1: Satellite = satellite1
        self.__satellite2: Satellite = satellite2
        self.__satellite3: Satellite = satellite3
        self.__amountOfObservations: int = amountOfObservations
        self.__amountOfHours: int = amountOfHours

    def drawVelocities(self) -> None:
        angularVelocitiesDrawer = AngularVelocitiesGraphDrawer(self.__satellite1, self.__satellite2, self.__satellite3,
                                                               self.__amountOfObservations)
        angularVelocitiesDrawer.draw()

        averageAngularVelocitiesDrawer = AverageAngularVelocitiesGraphDrawer(self.__satellite1, self.__satellite2,
                                                                             self.__satellite3, self.__amountOfHours)
        averageAngularVelocitiesDrawer.draw()

        linearVelocitiesDrawer = LinearVelocitiesGraphDrawer(self.__satellite1, self.__satellite2, self.__satellite3,
                                                             self.__amountOfObservations)
        linearVelocitiesDrawer.draw()

        averageLinearVelocitiesDrawer = AverageLinearVelocitiesGraphDrawer(self.__satellite1, self.__satellite2,
                                                                           self.__satellite3, self.__amountOfHours)
        averageLinearVelocitiesDrawer.draw()


class TemplateGraphDrawer(ABC):
    def __init__(self, amountOfObservations: int) -> None:
        self.amountOfObservations = amountOfObservations

    def draw(self) -> None:
        number1: int = self.getSatellite1Number()
        number2: int = self.getSatellite2Number()
        number3: int = self.getSatellite3Number()
        velocities1: list = self.getVelocities1()
        velocities2: list = self.getVelocities2()
        velocities3: list = self.getVelocities3()
        observations = np.arange(1, self.amountOfObservations + 1)
        plt.plot(observations, velocities1, 'o-', label="Спутник #" + str(number1))
        plt.plot(observations, velocities2, 'o-', label="Спутник #" + str(number2))
        plt.plot(observations, velocities3, 'o-', label="Спутник #" + str(number3))
        xLabel: str = self.getXLabel()
        plt.xlabel(xLabel)
        yLabel: str = self.getYLabel()
        plt.ylabel(yLabel)
        plt.legend()
        plt.grid(linestyle='-', linewidth=0.5)
        plt.show()

    @abstractmethod
    def getSatellite1Number(self) -> int:
        pass

    @abstractmethod
    def getSatellite2Number(self) -> int:
        pass

    @abstractmethod
    def getSatellite3Number(self) -> int:
        pass

    @abstractmethod
    def getVelocities1(self) -> list:
        pass

    @abstractmethod
    def getVelocities2(self) -> list:
        pass

    @abstractmethod
    def getVelocities3(self) -> list:
        pass

    @abstractmethod
    def getXLabel(self) -> str:
        pass

    @abstractmethod
    def getYLabel(self) -> str:
        pass


class AngularVelocitiesGraphDrawer(TemplateGraphDrawer):
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        super().__init__(amountOfObservations)
        self.__satellite1: Satellite = satellite1
        self.__satellite2: Satellite = satellite2
        self.__satellite3: Satellite = satellite3

    def getSatellite1Number(self) -> int:
        number: int = self.__satellite1.number
        return number

    def getSatellite2Number(self) -> int:
        number: int = self.__satellite2.number
        return number

    def getSatellite3Number(self) -> int:
        number: int = self.__satellite3.number
        return number

    def getVelocities1(self) -> list:
        velocities: list = self.__satellite1.angularVelocities
        return velocities

    def getVelocities2(self) -> list:
        velocities: list = self.__satellite2.angularVelocities
        return velocities

    def getVelocities3(self) -> list:
        velocities: list = self.__satellite3.angularVelocities
        return velocities

    def getXLabel(self) -> str:
        label: str = "Время"
        return label

    def getYLabel(self) -> str:
        label: str = "Угловая скорость, рад/c"
        return label


class AverageAngularVelocitiesGraphDrawer(TemplateGraphDrawer):
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        super().__init__(amountOfObservations)
        self.__satellite1: Satellite = satellite1
        self.__satellite2: Satellite = satellite2
        self.__satellite3: Satellite = satellite3

    def getSatellite1Number(self) -> int:
        number: int = self.__satellite1.number
        return number

    def getSatellite2Number(self) -> int:
        number: int = self.__satellite2.number
        return number

    def getSatellite3Number(self) -> int:
        number: int = self.__satellite3.number
        return number

    def getVelocities1(self) -> list:
        velocities: list = self.__satellite1.averageAngularVelocities
        return velocities

    def getVelocities2(self) -> list:
        velocities: list = self.__satellite2.averageAngularVelocities
        return velocities

    def getVelocities3(self) -> list:
        velocities: list = self.__satellite3.averageAngularVelocities
        return velocities

    def getXLabel(self) -> str:
        label: str = "Время, часы"
        return label

    def getYLabel(self) -> str:
        label: str = "Средняя угловая скорость, рад/c"
        return label


class LinearVelocitiesGraphDrawer(TemplateGraphDrawer):
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        super().__init__(amountOfObservations)
        self.__satellite1: Satellite = satellite1
        self.__satellite2: Satellite = satellite2
        self.__satellite3: Satellite = satellite3

    def getSatellite1Number(self) -> int:
        number: int = self.__satellite1.number
        return number

    def getSatellite2Number(self) -> int:
        number: int = self.__satellite2.number
        return number

    def getSatellite3Number(self) -> int:
        number: int = self.__satellite3.number
        return number

    def getVelocities1(self) -> list:
        velocities: list = self.__satellite1.linearVelocities
        return velocities

    def getVelocities2(self) -> list:
        velocities: list = self.__satellite2.linearVelocities
        return velocities

    def getVelocities3(self) -> list:
        velocities: list = self.__satellite3.linearVelocities
        return velocities

    def getXLabel(self) -> str:
        label: str = "Время"
        return label

    def getYLabel(self) -> str:
        label: str = "Линейная скорость, м/с"
        return label


class AverageLinearVelocitiesGraphDrawer(TemplateGraphDrawer):
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        super().__init__(amountOfObservations)
        self.__satellite1: Satellite = satellite1
        self.__satellite2: Satellite = satellite2
        self.__satellite3: Satellite = satellite3

    def getSatellite1Number(self) -> int:
        number: int = self.__satellite1.number
        return number

    def getSatellite2Number(self) -> int:
        number: int = self.__satellite2.number
        return number

    def getSatellite3Number(self) -> int:
        number: int = self.__satellite3.number
        return number

    def getVelocities1(self) -> list:
        velocities: list = self.__satellite1.averageLinearVelocities
        return velocities

    def getVelocities2(self) -> list:
        velocities: list = self.__satellite2.averageLinearVelocities
        return velocities

    def getVelocities3(self) -> list:
        velocities: list = self.__satellite3.averageLinearVelocities
        return velocities

    def getXLabel(self) -> str:
        label: str = "Время, часы"
        return label

    def getYLabel(self) -> str:
        label: str = "Средняя линейная скорость, рад/c"
        return label


def main():
    amountOfObservations: int = 360
    amountOfHours: int = 3

    fileName: str = "resources/WTZZ_6hours.dat"
    fileReader: FileReader = FileReader(fileName)

    satelliteFactory: SatelliteFactory = SatelliteFactory(fileReader, amountOfObservations)

    satellite1: Satellite = satelliteFactory.createSatellite("7", 1)
    satellite2: Satellite = satelliteFactory.createSatellite("2", 2, "2")
    satellite3: Satellite = satelliteFactory.createSatellite("2", 2, "3")

    consoleOutput: ConsoleOutput = ConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations,
                                                 amountOfHours)
    consoleOutput.printVelocities()

    graphDrawer: GraphDrawer = GraphDrawer(satellite1, satellite2, satellite3, amountOfObservations, amountOfHours)
    graphDrawer.drawVelocities()


if __name__ == '__main__':
    main()
