package Var1File;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws IOException {
        int amountOfObservations = 360;

        String fileName = "./src/Var1File/resources/POTS_6hours.dat";
        FileReader fileReader = new FileReader(fileName);

        SatelliteFactory satelliteFactory = new SatelliteFactory(fileReader, amountOfObservations);

        Satellite satellite1 = satelliteFactory.createSatellite('7', 1);
        Satellite satellite2 = satelliteFactory.createSatellite('2', '2', 2);
        Satellite satellite3 = satelliteFactory.createSatellite('2', '3', 2);

        ConsoleOutput consoleOutput = new ConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations);
        consoleOutput.printDelays();

        GraphDrawer.draw();
    }
}


class ConsoleOutput {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;
    private final int amountOfObservations;

    public ConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
        this.amountOfObservations = amountOfObservations;
    }

    public void printDelays() {
        int satellite1Number = satellite1.getNumber();
        int satellite2Number = satellite2.getNumber();
        int satellite3Number = satellite3.getNumber();
        System.out.println("Спутник #" + satellite1Number + "\tСпутник #" + satellite2Number + "\tСпутник #" + satellite3Number);
        DecimalFormat df = new DecimalFormat("#.##########");
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double delay1 = satellite1.getIonosphericDelayAt(observation);
            double delay2 = satellite2.getIonosphericDelayAt(observation);
            double delay3 = satellite3.getIonosphericDelayAt(observation);
            System.out.println(df.format(delay1) + "\t" + df.format(delay2) + "\t" + df.format(delay3));
        }
    }
}


class SatelliteFactory {
    private final int amountOfObservations;
    private final FileReader fileReader;


    public SatelliteFactory(FileReader fileReader, int amountOfObservations) {
        this.fileReader = fileReader;
        this.amountOfObservations = amountOfObservations;
    }

    public Satellite createSatellite(char satelliteNumber1, char satelliteNumber2, int satelliteNumberSize) {
        String satelliteNumberStr = Character.toString(satelliteNumber1) + Character.toString(satelliteNumber2);
        int satelliteNumeric = Integer.parseInt(satelliteNumberStr);
        ArrayList<Double> p1Array = new ArrayList<>(amountOfObservations);
        ArrayList<Double> p2Array = new ArrayList<>(amountOfObservations);
        ArrayList<ArrayList<Double>> measurements = fileReader.getMeasurements(satelliteNumber1, satelliteNumber2, satelliteNumberSize);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double p1Temp = measurements.get(observation).get(1);
            p1Array.add(p1Temp);
            double p2Temp = measurements.get(observation).get(2);
            p2Array.add(p2Temp);
        }
        PseudoRange p1 = new PseudoRange(p1Array);
        PseudoRange p2 = new PseudoRange(p2Array);
        IonosphericDelay delay = new IonosphericDelay(p1, p2);
        Satellite satellite = new Satellite(satelliteNumeric, delay);
        return satellite;
    }

    public Satellite createSatellite(char satelliteNumber, int satelliteNumberSize) {
        int satelliteNumeric = Character.getNumericValue(satelliteNumber);
        ArrayList<Double> p1Array = new ArrayList<>(amountOfObservations);
        ArrayList<Double> p2Array = new ArrayList<>(amountOfObservations);
        ArrayList<ArrayList<Double>> measurements = fileReader.getMeasurements(satelliteNumber, satelliteNumberSize);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double p1Temp = measurements.get(observation).get(1);
            double p2Temp = measurements.get(observation).get(2);
            p1Array.add(p1Temp);
            p2Array.add(p2Temp);
        }
        PseudoRange p1 = new PseudoRange(p1Array);
        PseudoRange p2 = new PseudoRange(p2Array);
        IonosphericDelay delay = new IonosphericDelay(p1, p2);
        Satellite satellite = new Satellite(satelliteNumeric, delay);
        return satellite;
    }
}


class Satellite {
    private final int number;
    private final IonosphericDelay ionosphericDelay;

    public Satellite(int number, IonosphericDelay ionosphericDelay) {
        this.number = number;
        this.ionosphericDelay = ionosphericDelay;
    }

    public int getNumber() {
        return number;
    }

    public double getIonosphericDelayAt(int observation) {
        return ionosphericDelay.getIonosphericDelayAt(observation);
    }
}


class IonosphericDelay {
    private final PseudoRange pseudoRange1;
    private final PseudoRange pseudoRange2;

    public IonosphericDelay(PseudoRange pseudoRange1, PseudoRange pseudoRange2) {
        this.pseudoRange1 = pseudoRange1;
        this.pseudoRange2 = pseudoRange2;
    }

    public double getIonosphericDelayAt(int interval) {
        double speedOfLight = 2.99792458 * 1E8;
        double p1 = pseudoRange1.getPseudoRangeAt(interval);
        double p2 = pseudoRange2.getPseudoRangeAt(interval);
        double k = getK();
        double delay = (p1 - p2) / (speedOfLight * (1 - k));
        double delayInMeters = delay * speedOfLight;
        return delayInMeters;
    }

    private double getK() {
        double f1 = 1_575_420_000;
        double f2 = 1_227_600_000;
        double k = Math.pow(f1, 2) / Math.pow(f2, 2);
        return k;
    }
}


class PseudoRange {
    private final ArrayList<Double> pseudoRanges;

    public PseudoRange(ArrayList<Double> pseudoRanges) {
        this.pseudoRanges = pseudoRanges;
    }

    public double getPseudoRangeAt(int observation) {
        return pseudoRanges.get(observation);
    }
}


class FileReader {
    private final byte[] allBytes;

    public FileReader(String fileName) throws IOException {
        FileInputStream fin = new FileInputStream(fileName);
        allBytes = new byte[fin.available()];
        int offset = 0;
        fin.read(allBytes, offset, allBytes.length);
    }

    public ArrayList<ArrayList<Double>> getMeasurements(char requiredSatelliteNumber1, char requiredSatelliteNumber2, int requiredSatelliteNumberSize) {
        ArrayList<ArrayList<ArrayList<Byte>>> lines = analyzeSyntaxAndReturnLinesList();
        ArrayList<ArrayList<Double>> measurements = new ArrayList<>();
        int numbersInLine = 21;
        for (ArrayList<ArrayList<Byte>> line : lines) {
            try {
                char satelliteNumber1 = (char)(byte)line.get(0).get(0);
                char satelliteNumber2 = (char)(byte)line.get(0).get(1);
                int satelliteNumberSize = line.get(0).size();

                if ((satelliteNumber1 == requiredSatelliteNumber1) &&
                    (satelliteNumber2 == requiredSatelliteNumber2) &&
                    (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    ArrayList<Double> lineOfNumbers = new ArrayList<>();
                    for (int number = 1; number <= numbersInLine; number++) {
                        double numeric = getNumeric(line, number);
                        lineOfNumbers.add(numeric);
                    }
                    measurements.add(lineOfNumbers);
                }
            } catch (Exception ignored) { }
        }
        return measurements;
    }

    public ArrayList<ArrayList<Double>> getMeasurements(char requiredSatelliteNumber, int requiredSatelliteNumberSize) {
        ArrayList<ArrayList<ArrayList<Byte>>> lines = analyzeSyntaxAndReturnLinesList();
        ArrayList<ArrayList<Double>> measurements = new ArrayList<>();
        int numbersInLine = 21;

        for (ArrayList<ArrayList<Byte>> line : lines) {
            try {
                char satelliteNumber = (char)(byte)line.get(0).get(0);
                int satelliteNumberSize = line.get(0).size();

                if ((satelliteNumber == requiredSatelliteNumber) && (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    ArrayList<Double> lineOfNumbers = new ArrayList<>();
                    for (int number = 1; number <= numbersInLine; number++) {
                        double numeric = getNumeric(line, number);
                        lineOfNumbers.add(numeric);
                    }
                    measurements.add(lineOfNumbers);
                }
            } catch (Exception ignored) { }
        }
        return measurements;
    }

    private double getNumeric(ArrayList<ArrayList<Byte>> line, int number) {
        StringBuilder numberBuilder = new StringBuilder();
        int numberLength = line.get(number).size();

        for (int digit = 0; digit < numberLength; digit++) {
            char symbol = (char)line.get(number).get(digit).byteValue();
            numberBuilder.append(symbol);
        }

        double numeric = Double.parseDouble(numberBuilder.toString());
        return numeric;
    }

    private ArrayList<ArrayList<ArrayList<Byte>>> analyzeSyntaxAndReturnLinesList() {
        ArrayList<ArrayList<ArrayList<Byte>>> lines = new ArrayList<>();
        ArrayList<ArrayList<Byte>> words = new ArrayList<>();
        ArrayList<Byte> symbols = new ArrayList<>();
        boolean isWord = false;

        byte tab = 9;
        byte newLine = 10;
        byte carriageReturn = 13;
        byte space = 32;

        for (byte symbol : allBytes) {
            if (symbol == newLine) {
                words.add(symbols);
                symbols = new ArrayList<>();
                lines.add(words);
                words = new ArrayList<>();
                isWord = false;
            } else if ((isWord) && (symbol == tab)) {
                words.add(symbols);
                symbols = new ArrayList<>();
                isWord = false;
            } else if ((symbol != space) && (symbol != tab) && (symbol != carriageReturn)) {
                isWord = true;
                symbols.add(symbol);
            }
        }
        return lines;
    }
}