package Var1File;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;


public class GraphDrawer extends Application {
    public static void draw() {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        int amountOfObservations = 360;

        String fileName = "./src/Var1File/resources/POTS_6hours.dat";
        FileReader fileReader = new FileReader(fileName);

        SatelliteFactory satelliteFactory = new SatelliteFactory(fileReader, amountOfObservations);

        Satellite satellite1 = satelliteFactory.createSatellite('7', 1);
        Satellite satellite2 = satelliteFactory.createSatellite('2', '2', 2);
        Satellite satellite3 = satelliteFactory.createSatellite('2', '3', 2);

        init(satellite1, satellite2, satellite3, amountOfObservations);
    }

    @SuppressWarnings("unchecked")
    private void init(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        Stage stage = new Stage();
        HBox root = new HBox();
        Scene scene = new Scene(root, 450, 330);

        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("Время");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Наклонная задержка сигнала, метры");

        LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);
        XYChart.Series<Number, Number> delay1 = new XYChart.Series<>();
        int satellite1Number = satellite1.getNumber();
        delay1.setName("Спутник #" + satellite1Number);
        XYChart.Series<Number, Number> delay2 = new XYChart.Series<>();
        int satellite2Number = satellite2.getNumber();
        delay2.setName("Спутник #" + satellite2Number);
        XYChart.Series<Number, Number> delay3 = new XYChart.Series<>();
        int satellite3Number = satellite3.getNumber();
        delay3.setName("Спутник #" + satellite3Number);

        for (int observation = 0; observation < amountOfObservations; observation++) {
            double delay1Data = satellite1.getIonosphericDelayAt(observation);
            delay1.getData().add(new XYChart.Data<>(observation, delay1Data));

            double delay2Data = satellite2.getIonosphericDelayAt(observation);
            delay2.getData().add(new XYChart.Data<>(observation, delay2Data));

            double delay3Data = satellite3.getIonosphericDelayAt(observation);
            delay3.getData().add(new XYChart.Data<>(observation, delay3Data));
        }

        lineChart.getData().addAll(delay1, delay2, delay3);
        root.getChildren().add(lineChart);

        stage.setScene(scene);
        stage.show();
    }
}
