package Var3Manual;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GraphDrawer extends Application {
    public static void draw() {
        launch();
    }

    @Override
    public void start(Stage primaryStage) {
        int amountOfObservations = 12;

        AxisGeo axisGeo = new AxisGeo(UserGeo.lonpp, IgpGeo.lon1, IgpGeo.lon2, UserGeo.latpp, IgpGeo.lat1, IgpGeo.lat2);
        WeightMatrix weightMatrix = new WeightMatrix(axisGeo);

        List<Double> alphaArray = Arrays.asList( 0.7451E-08, -0.1490E-07, -0.5960E-07,  0.1192E-06 );
        List<Double> betaArray = Arrays.asList( 0.9216E+05, -0.1147E+06, -0.1311E+06,  0.7209E+06 );
        List<Double> gpsTimeArray = Arrays.asList(
            80496.0, 90270.0, 93630.0, 100830.0, 108030.0, 0.0, 124230.0, 129630.0, 136800.0, 144000.0, 151200.0, 158400.0
        );

        IonCoefficients alpha = new IonCoefficients(alphaArray);
        IonCoefficients beta = new IonCoefficients(betaArray);
        GpsTime gpsTime = new GpsTime(gpsTimeArray);

        List<Integer> forecastA1 = Arrays.asList( 55, 90, 97, 98, 92, 80, 71, 72, 80, 72, 84, 65 );
        List<Integer> forecastA2 = Arrays.asList( 101, 87, 80, 62, 49, 54, 64, 58, 55, 57, 42, 52 );
        List<Integer> forecastA3 = Arrays.asList( 61, 92, 101, 101, 97, 85, 80, 74, 78, 73, 84, 68 );
        List<Integer> forecastA4 = Arrays.asList( 121, 102, 100, 74, 59, 63, 70, 66, 56, 56, 45, 75 );

        List<Integer> preciseA1 = Arrays.asList( 44, 77, 88, 82, 78, 64, 61, 62, 67, 63, 66, 55 );
        List<Integer> preciseA2 = Arrays.asList( 104, 79, 76, 56, 39, 33, 53, 48, 41, 47, 38, 45 );
        List<Integer> preciseA3 = Arrays.asList( 50, 82, 95, 87, 85, 69, 68, 68, 72, 68, 69, 60 );
        List<Integer> preciseA4 = Arrays.asList( 102, 89, 80, 65, 46, 43, 53, 50, 41, 42, 37, 64 );

        ArrayList<IonosphericDelay> forecastDelays = IonosphericDelaysFactory.createDelays(weightMatrix, forecastA1, forecastA2, forecastA3, forecastA4, amountOfObservations);
        ArrayList<IonosphericDelay> preciseDelays = IonosphericDelaysFactory.createDelays(weightMatrix, preciseA1, preciseA2, preciseA3, preciseA4, amountOfObservations);
        ArrayList<KlobucharModel> klobucharDelays = KlobucharDelaysFactory.createKlobuchar(gpsTime, alpha, beta, amountOfObservations);

        init(primaryStage, forecastDelays, preciseDelays, klobucharDelays, amountOfObservations);
    }

    @SuppressWarnings("unchecked")
    private void init(Stage primaryStage, ArrayList<IonosphericDelay> forecastDelays, ArrayList<IonosphericDelay> preciseDelays, ArrayList<KlobucharModel> klobucharDelays, int amountOfObservations) {
        HBox root = new HBox();
        Scene scene = new Scene(root, 450, 330);

        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("Время, час");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Ионосферная поправка, метр");

        LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);
        XYChart.Series<Number, Number> forecastData = new XYChart.Series<>();
        forecastData.setName("igrg");
        XYChart.Series<Number, Number> preciseData = new XYChart.Series<>();
        preciseData.setName("igsg");
        XYChart.Series<Number, Number> klobucharData = new XYChart.Series<>();
        klobucharData.setName("Klobuchar");

        for (int observation = 0; observation < amountOfObservations; observation++) {
            double forecastDelay = forecastDelays.get(observation).getDelayInMeters();
            forecastData.getData().add(new XYChart.Data<>(observation, forecastDelay));

            double preciseDelay = preciseDelays.get(observation).getDelayInMeters();
            preciseData.getData().add(new XYChart.Data<>(observation, preciseDelay));

            double klobucharDelay = klobucharDelays.get(observation).getKlobucharDelayInMeters();
            klobucharData.getData().add(new XYChart.Data<>(observation, klobucharDelay));
        }

        lineChart.getData().addAll(forecastData, preciseData, klobucharData);
        root.getChildren().add(lineChart);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
