package Var3Manual;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        int amountOfObservations = 12;

        AxisGeo axisGeo = new AxisGeo(UserGeo.lonpp, IgpGeo.lon1, IgpGeo.lon2, UserGeo.latpp, IgpGeo.lat1, IgpGeo.lat2);
        WeightMatrix weightMatrix = new WeightMatrix(axisGeo);

        List<Double> alphaArray = Arrays.asList( 0.7451E-08, -0.1490E-07, -0.5960E-07,  0.1192E-06 );
        List<Double> betaArray = Arrays.asList( 0.9216E+05, -0.1147E+06, -0.1311E+06,  0.7209E+06 );
        List<Double> gpsTimeArray = Arrays.asList(
            80496.0, 90270.0, 93630.0, 100830.0, 108030.0, 0.0, 124230.0, 129630.0, 136800.0, 144000.0, 151200.0, 158400.0
        );

        IonCoefficients alpha = new IonCoefficients(alphaArray);
        IonCoefficients beta = new IonCoefficients(betaArray);
        GpsTime gpsTime = new GpsTime(gpsTimeArray);

        List<Integer> forecastA1 = Arrays.asList( 72, 81, 86, 73, 47, 52, 55, 57, 58, 56, 46, 38 );
        List<Integer> forecastA2 = Arrays.asList( 53, 84, 86, 80, 59, 54, 54, 60, 67, 57, 54, 45 );
        List<Integer> forecastA3 = Arrays.asList( 80, 108, 92, 98, 79, 73, 76, 76, 73, 66, 65, 55 );
        List<Integer> forecastA4 = Arrays.asList( 88, 108, 91, 97, 70, 72, 73, 74, 69, 63, 54, 55 );

        List<Integer> preciseA1 = Arrays.asList( 69, 78, 80, 64, 36, 42, 42, 52, 51, 49, 42, 37 );
        List<Integer> preciseA2 = Arrays.asList( 51, 80, 81, 68, 45, 47, 41, 51, 56, 51, 49, 41 );
        List<Integer> preciseA3 = Arrays.asList( 63, 97, 80, 87, 60, 55, 58, 59, 57, 58, 50, 39 );
        List<Integer> preciseA4 = Arrays.asList( 70, 98, 74, 82, 48, 51, 52, 56, 48, 49, 35, 39 );

        ArrayList<IonosphericDelay> forecastDelays = IonosphericDelaysFactory.createDelays(weightMatrix, forecastA1, forecastA2, forecastA3, forecastA4, amountOfObservations);
        ArrayList<IonosphericDelay> preciseDelays = IonosphericDelaysFactory.createDelays(weightMatrix, preciseA1, preciseA2, preciseA3, preciseA4, amountOfObservations);
        ArrayList<KlobucharModel> klobucharDelays = KlobucharDelaysFactory.createKlobuchar(gpsTime, alpha, beta, amountOfObservations);

        ConsoleOutput consoleOutput = new ConsoleOutput(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations);
        consoleOutput.printDelays();

        GraphDrawer.draw();
    }
}


class ConsoleOutput {
    private final ArrayList<IonosphericDelay> forecastDelays;
    private final ArrayList<IonosphericDelay> preciseDelays;
    private final ArrayList<KlobucharModel> klobucharDelays;
    private final int amountOfObservations;

    public ConsoleOutput(ArrayList<IonosphericDelay> forecastDelays, ArrayList<IonosphericDelay> preciseDelays, ArrayList<KlobucharModel> klobucharDelays, int amountOfObservations) {
        this.forecastDelays = forecastDelays;
        this.preciseDelays = preciseDelays;
        this.klobucharDelays = klobucharDelays;
        this.amountOfObservations = amountOfObservations;
    }

    public void printDelays() {
        System.out.println("igrg\tigsg\tklobuchar");
        DecimalFormat df = new DecimalFormat("#.###");
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double forecastValue = forecastDelays.get(observation).getDelayInMeters();
            double preciseValue = preciseDelays.get(observation).getDelayInMeters();
            double klobucharValue = klobucharDelays.get(observation).getKlobucharDelayInMeters();
            System.out.println(df.format(forecastValue) + "\t" + df.format(preciseValue) + "\t" + klobucharValue);
        }
    }
}


class IonosphericDelaysFactory {
    public static ArrayList<IonosphericDelay> createDelays(WeightMatrix weightMatrix, List<Integer> tecA1, List<Integer> tecA2,
                                                           List<Integer> tecA3, List<Integer> tecA4, int amountOfObservations) {
        ArrayList<IonosphericDelay> delays = new ArrayList<>(amountOfObservations);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            int a1 = tecA1.get(observation);
            int a2 = tecA2.get(observation);
            int a3 = tecA3.get(observation);
            int a4 = tecA4.get(observation);

            List<Integer> tecArray = Arrays.asList(a1, a2, a3, a4);

            Tec tempTec = new Tec(tecArray);
            IonosphericDelay tempDelay = new IonosphericDelay(weightMatrix, tempTec);
            delays.add(tempDelay);
        }
        return delays;
    }
}


class KlobucharDelaysFactory {
    public static ArrayList<KlobucharModel> createKlobuchar(GpsTime gpsTime, IonCoefficients alpha, IonCoefficients beta, int amountOfObservations) {
        ArrayList<KlobucharModel> models = new ArrayList<>(amountOfObservations);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double time = gpsTime.getGpsTimeAt(observation);
            KlobucharModel klobucharTemp = new KlobucharModel(time, alpha, beta);
            models.add(klobucharTemp);
        }
        return models;
    }
}


class UserGeo {
    private static final double halfCircle = 180;
    public static double latpp = 48 / halfCircle;
    public static double lonpp = 135 / halfCircle;
}


class IgpGeo {
    private static final double halfCircle = 180;
    public static double lon1 = 130 / halfCircle;
    public static double lon2 = 140 / halfCircle;
    public static double lat1 = 45 / halfCircle;
    public static double lat2 = 55 / halfCircle;
}


class AxisGeo {
    private final double xpp;
    private final double ypp;

    public AxisGeo(double lonpp, double lon1, double lon2, double latpp, double lat1, double lat2) {
        this.xpp = (lonpp - lon1) / (lon2 - lon1);
        this.ypp = (latpp - lat1) / (lat2 - lat1);
    }

    public double getXpp() {
        return xpp;
    }

    public double getYpp() {
        return ypp;
    }
}


class WeightMatrix {
    private final ArrayList<Double> weights;

    public WeightMatrix(AxisGeo axisGeo) {
        this.weights = new ArrayList<>(4);
        double xpp = axisGeo.getXpp();
        double ypp = axisGeo.getYpp();
        weights.add(xpp * ypp);
        weights.add((1 - xpp) * ypp);
        weights.add((1 - xpp) * (1 - ypp));
        weights.add(xpp * (1 - ypp));
    }

    public double getWeightAt(int pos) {
        return weights.get(pos);
    }
}


class IonCoefficients {
    private final List<Double> coefficients;

    public IonCoefficients(List<Double> coefficients) {
        this.coefficients = coefficients;
    }

    public double getCoefficientAt(int pos) {
        return coefficients.get(pos);
    }
}


class Tec {
    private final List<Integer> tec;

    public Tec(List<Integer> tec) {
        this.tec = tec;
    }

    public double getTecAt(int pos) {
        return tec.get(pos);
    }
}


class GpsTime {
    private final List<Double> gpsTime;

    public GpsTime(List<Double> gpsTime) {
        this.gpsTime = gpsTime;
    }

    public double getGpsTimeAt(int pos) {
        return gpsTime.get(pos);
    }
}


class IonosphericDelay {
    private final WeightMatrix weightMatrix;
    private final Tec tec;

    public IonosphericDelay(WeightMatrix weightMatrix, Tec tec) {
        this.weightMatrix = weightMatrix;
        this.tec = tec;
    }

    public double getDelayInMeters() {
        double tecuToMetersCoefficient = getTecuToMetersCoefficient();
        double delayInTecu = getDelayInTecu();
        double delayInMeters = delayInTecu * tecuToMetersCoefficient;
        return delayInMeters;
    }

    private double getTecuToMetersCoefficient() {
        double l1 = 1_575_420_000;
        double oneTecUnit = 1E16;
        double coefficient = 40.3 / Math.pow(l1, 2) * oneTecUnit;
        return coefficient;
    }

    private double getDelayInTecu() {
        double delay = 0;
        for (int observation = 0; observation < 4; observation++) {
            double weight = weightMatrix.getWeightAt(observation);
            double rawTec = tec.getTecAt(observation);
            double tecInOneTecUnit = rawTec * 0.1;
            delay += (weight * tecInOneTecUnit);
        }
        return delay;
    }
}


class KlobucharModel {
    private final double elevationAngle;
    private final double azimuth;
    private final double gpsTime;
    private final IonCoefficients alpha;
    private final IonCoefficients beta;

    public KlobucharModel(double gpsTime, IonCoefficients alpha, IonCoefficients beta) {
        double halfCircle = 180;
        this.gpsTime = gpsTime;
        this.elevationAngle = 90 / halfCircle;
        this.azimuth = 0;
        this.alpha = alpha;
        this.beta = beta;
    }

    public double getKlobucharDelayInMeters() {
        double delayInSeconds = getKlobucharDelayInSeconds();
        double speedOfLight = 2.99792458 * 1E8;
        double delayInMeters = delayInSeconds * speedOfLight;
        return delayInMeters;
    }

    private double getEarthCenteredAngle() {
        double earthCenteredAngle = 0.0137 / (elevationAngle + 0.11) - 0.022;
        return earthCenteredAngle;
    }

    private double getIppLatitude() {
        double latpp = UserGeo.latpp;
        double earthCenteredAngle = getEarthCenteredAngle();
        double ippLatitude = latpp + earthCenteredAngle * Math.cos(azimuth);
        if (ippLatitude > 0.416)
            ippLatitude = 0.416;
        else if (ippLatitude < -0.416)
            ippLatitude = -0.416;
        return ippLatitude;
    }

    private double getIppLongtitude() {
        double lonpp = UserGeo.lonpp;
        double earthCenteredAngle = getEarthCenteredAngle();
        double ippLatitude = getIppLatitude();
        double ippLongtitude = lonpp + (earthCenteredAngle * Math.sin(azimuth) / (Math.cos(ippLatitude)));
        return ippLongtitude;
    }

    private double getIppGeomagneticLatitude() {
        double ippLatitude = getIppLatitude();
        double ippLongtitude = getIppLongtitude();
        double ippGeomagneticLatitude = ippLatitude + 0.064 * Math.cos(ippLongtitude - 1.617);
        return ippGeomagneticLatitude;
    }

    private double getIppLocalTime() {
        double secondsInOneDay = 86_400;
        double secondsInTwelveHours = 43_200;
        double ippLongtitude = getIppLongtitude();
        double ippLocalTime = secondsInTwelveHours * ippLongtitude + gpsTime;
        while (ippLocalTime > secondsInOneDay)
            ippLocalTime -= secondsInOneDay;
        while (ippLocalTime < 0)
            ippLocalTime += secondsInOneDay;
        return ippLocalTime;
    }

    private double getIonosphericDelayAmplitude() {
        double ippGeomagneticLatitude = getIppGeomagneticLatitude();
        double amplitude = 0;
        for (int i = 0; i < 4; i++) {
            amplitude += (alpha.getCoefficientAt(i) * Math.pow(ippGeomagneticLatitude, i));
        }
        if (amplitude < 0)
            amplitude = 0;
        return amplitude;
    }

    private double getIonosphericDelayPeriod() {
        double ippGeomagneticLatitude = getIppGeomagneticLatitude();
        double period = 0;
        for (int i = 0; i < 4; i++) {
            period += (beta.getCoefficientAt(i) * Math.pow(ippGeomagneticLatitude, i));
        }
        if (period < 72_000)
            period = 72_000;
        return period;
    }

    private double getIonosphericDelayPhase() {
        double ippLocalTime = getIppLocalTime();
        double ionosphericDelayPeriod = getIonosphericDelayPeriod();
        double ionosphericDelayPhase = 2 * Math.PI * (ippLocalTime - 50_400) / ionosphericDelayPeriod;
        return ionosphericDelayPhase;
    }

    private double getSlantFactor() {
        double slantFactor = 1 + 16 * Math.pow((0.53 - elevationAngle), 3);
        return slantFactor;
    }

    private double getKlobucharDelayInSeconds() {
        double ionosphericDelayPhase = getIonosphericDelayPhase();
        double ionosphericDelayAmplitude = getIonosphericDelayAmplitude();
        double slantFactor = getSlantFactor();
        double ionosphericTimeDelay;
        if (Math.abs(ionosphericDelayPhase) > 1.57)
            ionosphericTimeDelay = 5E-9 * slantFactor;
        else
            ionosphericTimeDelay = (5E-9 + ionosphericDelayAmplitude * (1 - Math.pow(ionosphericDelayPhase, 2) / 2 + Math.pow(ionosphericDelayPhase, 4) / 24)) * slantFactor;
        return ionosphericTimeDelay;
    }
}