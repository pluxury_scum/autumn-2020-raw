package Var2File;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import java.util.ArrayList;

public class GraphDrawer extends Application {
    public static void draw() {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        int amountOfObservations = 360;
        int amountOfHours = 3;

        String fileName = "./src/Var2File/resources/POTS_6hours.dat";
        FileReader fileReader = new FileReader(fileName);

        SatelliteFactory satelliteFactory = new SatelliteFactory(fileReader, amountOfObservations);

        Satellite satellite1 = satelliteFactory.createSatellite('7', 1);
        Satellite satellite2 = satelliteFactory.createSatellite('2', '2', 2);
        Satellite satellite3 = satelliteFactory.createSatellite('2', '3', 2);

        init(satellite1, satellite2, satellite3, amountOfObservations, amountOfHours);
    }

    private void init(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations, int amountOfHours) {
        TemplateDrawer angularVelocitiesDrawer = new AngularVelocitiesDrawer(satellite1, satellite2, satellite3, amountOfObservations);
        angularVelocitiesDrawer.draw();

        TemplateDrawer averageAngularVelocitiesDrawer = new AverageAngularVelocitiesDrawer(satellite1, satellite2, satellite3, amountOfHours);
        averageAngularVelocitiesDrawer.draw();

        TemplateDrawer linearVelocitiesDrawer = new LinearVelocitiesDrawer(satellite1, satellite2, satellite3, amountOfObservations);
        linearVelocitiesDrawer.draw();

        TemplateDrawer averageLinearVelocitiesDrawer = new AverageLinearVelocitiesDrawer(satellite1, satellite2, satellite3, amountOfHours);
        averageLinearVelocitiesDrawer.draw();
    }
}


abstract class TemplateDrawer {
    private final int amountOfObservations;

    public TemplateDrawer(int amountOfObservations) {
        this.amountOfObservations = amountOfObservations;
    }

    @SuppressWarnings("unchecked")
    protected void draw() {
        Stage stage = new Stage();
        HBox hbox = new HBox();
        Scene scene = new Scene(hbox, 450, 330);

        NumberAxis xAxis = new NumberAxis();
        String xAxisLabel = getXAxisLabel();
        xAxis.setLabel(xAxisLabel);

        NumberAxis yAxis = new NumberAxis();
        String yAxisLabel = getYAxisLabel();
        yAxis.setLabel(yAxisLabel);

        LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);

        XYChart.Series<Number, Number> velocities1 = new XYChart.Series<>();
        int satellite1Number = getSatellite1Number();
        velocities1.setName("Спутник #" + satellite1Number);
        XYChart.Series<Number, Number> velocities2 = new XYChart.Series<>();
        int satellite2Number = getSatellite2Number();
        velocities2.setName("Спутник #" + satellite2Number);
        XYChart.Series<Number, Number> velocities3 = new XYChart.Series<>();
        int satellite3Number = getSatellite3Number();
        velocities3.setName("Спутник #" + satellite3Number);

        ArrayList<Double> velocities1Array = getVelocities1();
        ArrayList<Double> velocities2Array = getVelocities2();
        ArrayList<Double> velocities3Array = getVelocities3();

        for (int observation = 0; observation < amountOfObservations; observation++) {
            double satellite1Velocity = velocities1Array.get(observation);
            velocities1.getData().add(new XYChart.Data<>(observation, satellite1Velocity));

            double satellite2Velocity = velocities2Array.get(observation);
            velocities2.getData().add(new XYChart.Data<>(observation, satellite2Velocity));

            double satellite3Velocity = velocities3Array.get(observation);
            velocities3.getData().add(new XYChart.Data<>(observation, satellite3Velocity));
        }

        lineChart.getData().addAll(velocities1, velocities2, velocities3);
        hbox.getChildren().add(lineChart);

        stage.setScene(scene);
        stage.show();
    }

    abstract String getXAxisLabel();
    abstract String getYAxisLabel();
    abstract int getSatellite1Number();
    abstract int getSatellite2Number();
    abstract int getSatellite3Number();
    abstract ArrayList<Double> getVelocities1();
    abstract ArrayList<Double> getVelocities2();
    abstract ArrayList<Double> getVelocities3();
}


class AngularVelocitiesDrawer extends TemplateDrawer {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;

    public AngularVelocitiesDrawer(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(amountOfObservations);
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
    }

    @Override
    String getXAxisLabel() {
        String label = "Время";
        return label;
    }

    @Override
    String getYAxisLabel() {
        String label = "Угловая скорость, рад/c";
        return label;
    }

    @Override
    int getSatellite1Number() {
        int number = satellite1.getNumber();
        return number;
    }

    @Override
    int getSatellite2Number() {
        int number = satellite2.getNumber();
        return number;
    }

    @Override
    int getSatellite3Number() {
        int number = satellite3.getNumber();
        return number;
    }

    @Override
    ArrayList<Double> getVelocities1() {
        ArrayList<Double> velocities1 = satellite1.getAngularVelocities();
        return velocities1;
    }

    @Override
    ArrayList<Double> getVelocities2() {
        ArrayList<Double> velocities2 = satellite2.getAngularVelocities();
        return velocities2;
    }

    @Override
    ArrayList<Double> getVelocities3() {
        ArrayList<Double> velocities3 = satellite3.getAngularVelocities();
        return velocities3;
    }
}


class AverageAngularVelocitiesDrawer extends TemplateDrawer {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;

    public AverageAngularVelocitiesDrawer(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(amountOfObservations);
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
    }

    @Override
    String getXAxisLabel() {
        String label = "Время, часы";
        return label;
    }

    @Override
    String getYAxisLabel() {
        String label = "Средняя угловая скорость, рад/c";
        return label;
    }

    @Override
    int getSatellite1Number() {
        int number = satellite1.getNumber();
        return number;
    }

    @Override
    int getSatellite2Number() {
        int number = satellite2.getNumber();
        return number;
    }

    @Override
    int getSatellite3Number() {
        int number = satellite3.getNumber();
        return number;
    }

    @Override
    ArrayList<Double> getVelocities1() {
        ArrayList<Double> velocities1 = satellite1.getAverageAngularVelocities();
        return velocities1;
    }

    @Override
    ArrayList<Double> getVelocities2() {
        ArrayList<Double> velocities2 = satellite2.getAverageAngularVelocities();
        return velocities2;
    }

    @Override
    ArrayList<Double> getVelocities3() {
        ArrayList<Double> velocities3 = satellite3.getAverageAngularVelocities();
        return velocities3;
    }
}


class LinearVelocitiesDrawer extends TemplateDrawer {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;

    public LinearVelocitiesDrawer(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(amountOfObservations);
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
    }

    @Override
    String getXAxisLabel() {
        String label = "Время";
        return label;
    }

    @Override
    String getYAxisLabel() {
        String label = "Линейная скорость, м/с";
        return label;
    }

    @Override
    int getSatellite1Number() {
        int number = satellite1.getNumber();
        return number;
    }

    @Override
    int getSatellite2Number() {
        int number = satellite2.getNumber();
        return number;
    }

    @Override
    int getSatellite3Number() {
        int number = satellite3.getNumber();
        return number;
    }

    @Override
    ArrayList<Double> getVelocities1() {
        ArrayList<Double> velocities1 = satellite1.getLinearVelocities();
        return velocities1;
    }

    @Override
    ArrayList<Double> getVelocities2() {
        ArrayList<Double> velocities2 = satellite2.getLinearVelocities();
        return velocities2;
    }

    @Override
    ArrayList<Double> getVelocities3() {
        ArrayList<Double> velocities3 = satellite3.getLinearVelocities();
        return velocities3;
    }
}


class AverageLinearVelocitiesDrawer extends TemplateDrawer {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;

    public AverageLinearVelocitiesDrawer(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(amountOfObservations);
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
    }

    @Override
    String getXAxisLabel() {
        String label = "Время, часы";
        return label;
    }

    @Override
    String getYAxisLabel() {
        String label = "Средняя линейная скорость, рад/c";
        return label;
    }

    @Override
    int getSatellite1Number() {
        int number = satellite1.getNumber();
        return number;
    }

    @Override
    int getSatellite2Number() {
        int number = satellite2.getNumber();
        return number;
    }

    @Override
    int getSatellite3Number() {
        int number = satellite3.getNumber();
        return number;
    }

    @Override
    ArrayList<Double> getVelocities1() {
        ArrayList<Double> velocities1 = satellite1.getAverageLinearVelocities();
        return velocities1;
    }

    @Override
    ArrayList<Double> getVelocities2() {
        ArrayList<Double> velocities2 = satellite2.getAverageLinearVelocities();
        return velocities2;
    }

    @Override
    ArrayList<Double> getVelocities3() {
        ArrayList<Double> velocities3 = satellite3.getAverageLinearVelocities();
        return velocities3;
    }
}