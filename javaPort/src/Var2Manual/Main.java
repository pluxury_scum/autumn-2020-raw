package Var2Manual;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        int amountOfObservations = 360;
        int amountOfHours = 3;

        List<Double> elevationAngles1 = Arrays.asList(
            10.097942, 10.335569, 10.573393, 10.81141, 11.049618, 11.288012, 11.52659, 11.765349, 12.004284, 12.243393,
            12.482673, 12.722119, 12.961729, 13.201498, 13.441423, 13.6815, 13.921726, 14.162097, 14.402609, 14.643258,
            14.88404, 15.124951, 15.365988, 15.607145, 15.848419, 16.089806, 16.331302, 16.572901, 16.8146, 17.056394,
            17.298278, 17.540249, 17.7823, 18.024429, 18.266629, 18.508895, 18.751224, 18.99361, 19.236047, 19.478531,
            19.721057, 19.963618, 20.20621, 20.448827, 20.691464, 20.934114, 21.176773, 21.419433, 21.662091, 21.904738,
            22.14737, 22.38998, 22.632562, 22.875109, 23.117615, 23.360073, 23.602478, 23.844821, 24.087096, 24.329296,
            24.571414, 24.813443, 25.055375, 25.297204, 25.538921, 25.780519, 26.02199, 26.263326, 26.504519, 26.745562,
            26.986445, 27.227161, 27.467701, 27.708056, 27.948218, 28.188178, 28.427926, 28.667454, 28.906752, 29.145811,
            29.384621, 29.623173, 29.861456, 30.099461, 30.337177, 30.574594, 30.811702, 31.04849, 31.284947, 31.521062,
            31.756824, 31.992222, 32.227245, 32.46188, 32.696115, 32.92994, 33.163341, 33.396307, 33.628825, 33.860882,
            34.092465, 34.323561, 34.554158, 34.78424, 35.013796, 35.242811, 35.471271, 35.699161, 35.926468, 36.153177,
            36.379272, 36.604739, 36.829563, 37.053727, 37.277217, 37.500016, 37.722109, 37.943478, 38.164108, 38.383982,
            38.603082, 38.821391, 39.038892, 39.255567, 39.471399, 39.686368, 39.900458, 40.113648, 40.32592, 40.537256,
            40.747636, 40.95704, 41.165448, 41.372841, 41.579198, 41.784499, 41.988723, 42.191849, 42.393855, 42.594722,
            42.794426, 42.992946, 43.190259, 43.386345, 43.581179, 43.774739, 43.967003, 44.157946, 44.347546, 44.535779,
            44.72262, 44.908047, 45.092035, 45.274558, 45.455594, 45.635116, 45.8131, 45.98952, 46.164353, 46.33757,
            46.509149, 46.679061, 46.847282, 47.013786, 47.178546, 47.341536, 47.50273, 47.662101, 47.819623, 47.975269,
            48.129012, 48.280827, 48.430685, 48.57856, 48.724426, 48.868256, 49.010023, 49.1497, 49.287261, 49.422679,
            49.555928, 49.686981, 49.815812, 49.942396, 50.066705, 50.188714, 50.308397, 50.42573, 50.540687, 50.653243,
            50.763373, 50.871053, 50.976259, 51.078966, 51.179153, 51.276795, 51.371871, 51.464356, 51.554231, 51.641474,
            51.726062, 51.807978, 51.887199, 51.963707, 52.037483, 52.108509, 52.176767, 52.24224, 52.304912, 52.364766,
            52.421788, 52.475963, 52.527277, 52.575718, 52.621272, 52.663929, 52.703677, 52.740506, 52.774407, 52.805372,
            52.833392, 52.858462, 52.880574, 52.899723, 52.915905, 52.929117, 52.939355, 52.946618, 52.950904, 52.952214,
            52.950548, 52.945907, 52.938293, 52.927711, 52.914163, 52.897656, 52.878193, 52.855782, 52.830431, 52.802146,
            52.770938, 52.736816, 52.69979, 52.659872, 52.617073, 52.571406, 52.522885, 52.471524, 52.417338, 52.360342,
            52.300553, 52.237986, 52.172661, 52.104594, 52.033804, 51.960311, 51.884134, 51.805294, 51.723811, 51.639707,
            51.553003, 51.463721, 51.371885, 51.277516, 51.180639, 51.081277, 50.979455, 50.875196, 50.768525, 50.659467,
            50.548048, 50.434293, 50.318227, 50.199877, 50.079269, 49.956428, 49.831382, 49.704156, 49.574778, 49.443273,
            49.30967, 49.173994, 49.036273, 48.896533, 48.754802, 48.611106, 48.465473, 48.317929, 48.168501, 48.017215,
            47.8641, 47.70918, 47.552484, 47.394037, 47.233865, 47.071996, 46.908455, 46.743268, 46.57646, 46.408059,
            46.238088, 46.066574, 45.893542, 45.719016, 45.543022, 45.365585, 45.186727, 45.006475, 44.824851, 44.64188,
            44.457585, 44.27199, 44.085118, 43.896991, 43.707633, 43.517065, 43.325311, 43.132391, 42.938328, 42.743144,
            42.546859, 42.349495, 42.151072, 41.951611, 41.751132, 41.549656, 41.347201, 41.143789, 40.939437, 40.734166,
            40.527994, 40.320939, 40.11302, 39.904255, 39.694663, 39.484259, 39.273063, 39.061091, 38.84836, 38.634887,
            38.420689, 38.205781, 37.990179, 37.7739, 37.556959, 37.339371, 37.121152, 36.902316, 36.682878, 36.462852,
            36.242253, 36.021095, 35.799392, 35.577157, 35.354404, 35.131145, 34.907395, 34.683166, 34.45847, 34.233319
        );

        List<Double> elevationAngles2 = Arrays.asList(
            10.151129, 10.378736, 10.60649, 10.834391, 11.062439, 11.290635, 11.518977, 11.747467, 11.976104, 12.204888,
            12.433818, 12.662895, 12.892119, 13.12149, 13.351007, 13.580671, 13.810481, 14.040437, 14.270539, 14.500788,
            14.731182, 14.961723, 15.192409, 15.42324, 15.654217, 15.885339, 16.116607, 16.348019, 16.579577, 16.811279,
            17.043126, 17.275117, 17.507252, 17.739532, 17.971955, 18.204522, 18.437233, 18.670087, 18.903084, 19.136224,
            19.369507, 19.602932, 19.836499, 20.070209, 20.30406, 20.538053, 20.772187, 21.006462, 21.240878, 21.475435,
            21.710131, 21.944968, 22.179944, 22.41506, 22.650314, 22.885708, 23.12124, 23.356909, 23.592717, 23.828662,
            24.064744, 24.300963, 24.537318, 24.773809, 25.010436, 25.247198, 25.484094, 25.721125, 25.95829, 26.195589,
            26.43302, 26.670585, 26.908281, 27.146109, 27.384069, 27.622159, 27.860379, 28.09873, 28.337209, 28.575818,
            28.814554, 29.053419, 29.29241, 29.531528, 29.770772, 30.010142, 30.249636, 30.489255, 30.728997, 30.968862,
            31.20885, 31.448959, 31.689189, 31.92954, 32.17001, 32.410599, 32.651307, 32.892132, 33.133073, 33.374131,
            33.615304, 33.856592, 34.097993, 34.339507, 34.581133, 34.822871, 35.064719, 35.306676, 35.548742, 35.790916,
            36.033196, 36.275583, 36.518074, 36.760669, 37.003368, 37.246168, 37.489069, 37.732071, 37.975171, 38.218369,
            38.461664, 38.705055, 38.94854, 39.192119, 39.435789, 39.679551, 39.923403, 40.167344, 40.411372, 40.655486,
            40.899685, 41.143967, 41.388332, 41.632778, 41.877303, 42.121906, 42.366586, 42.611341, 42.85617, 43.101071,
            43.346043, 43.591084, 43.836193, 44.081368, 44.326607, 44.571908, 44.817271, 45.062693, 45.308172, 45.553707,
            45.799296, 46.044937, 46.290628, 46.536367, 46.782152, 47.027981, 47.273853, 47.519765, 47.765714, 48.0117,
            48.257719, 48.503769, 48.749849, 48.995955, 49.242086, 49.488238, 49.73441, 49.980599, 50.226803, 50.473018,
            50.719242, 50.965473, 51.211707, 51.457942, 51.704175, 51.950403, 52.196622, 52.44283, 52.689024, 52.9352,
            53.181356, 53.427487, 53.67359, 53.919662, 54.165699, 54.411697, 54.657653, 54.903563, 55.149422, 55.395228,
            55.640975, 55.88666, 56.132277, 56.377824, 56.623294, 56.868684, 57.113989, 57.359203, 57.604322, 57.849341,
            58.094253, 58.339055, 58.58374, 58.828303, 59.072737, 59.317037, 59.561196, 59.805208, 60.049067, 60.292766,
            60.536297, 60.779655, 61.02283, 61.265817, 61.508606, 61.751191, 61.993562, 62.235712, 62.477632, 62.719312,
            62.960743, 63.201916, 63.442821, 63.683447, 63.923784, 64.163821, 64.403547, 64.64295, 64.882019, 65.120739,
            65.3591, 65.597087, 65.834687, 66.071885, 66.308667, 66.545017, 66.78092, 67.016358, 67.251316, 67.485775,
            67.719716, 67.953122, 68.185971, 68.418244, 68.649919, 68.880974, 69.111386, 69.341131, 69.570184, 69.798519,
            70.026109, 70.252926, 70.478941, 70.704123, 70.928441, 71.151861, 71.374349, 71.595869, 71.816383, 72.035853,
            72.254237, 72.471494, 72.687578, 72.902443, 73.116042, 73.328322, 73.539232, 73.748717, 73.956719, 74.163178,
            74.368031, 74.571213, 74.772655, 74.972287, 75.170033, 75.365816, 75.559555, 75.751165, 75.940559, 76.127644,
            76.312325, 76.494502, 76.674073, 76.850929, 77.024959, 77.196047, 77.364073, 77.528914, 77.69044, 77.84852,
            78.003017, 78.153792, 78.3007, 78.443593, 78.582323, 78.716736, 78.846675, 78.971984, 79.092505, 79.208077,
            79.31854, 79.423737, 79.523509, 79.617702, 79.706164, 79.788748, 79.865311, 79.935719, 79.999844, 80.057565,
            80.108774, 80.153371, 80.191268, 80.222391, 80.246677, 80.264078, 80.27456, 80.278103, 80.274705, 80.264375,
            80.247139, 80.223036, 80.192122, 80.154465, 80.110145, 80.059255, 80.0019, 79.938194, 79.868262, 79.792236,
            79.710255, 79.622465, 79.529017, 79.430065, 79.325767, 79.216283, 79.101773, 78.9824, 78.858324, 78.729705,
            78.596703, 78.459473, 78.31817, 78.172944, 78.023944, 77.871313, 77.715191, 77.555716, 77.393019, 77.227228,
            77.058468, 76.886858, 76.712513, 76.535546, 76.356063, 76.174167, 75.989957, 75.803528, 75.614972, 75.424376
        );

        List<Double> elevationAngles3 = Arrays.asList(
            10.037421, 10.245897, 10.454511, 10.663267, 10.872163, 11.0812, 11.290379, 11.499701, 11.709165, 11.918773,
            12.128525, 12.338421, 12.548462, 12.758649, 12.968982, 13.179462, 13.390089, 13.600863, 13.811785, 14.022856,
            14.234077, 14.445447, 14.656967, 14.868638, 15.080461, 15.292435, 15.504561, 15.71684, 15.929273, 16.14186,
            16.3546, 16.567496, 16.780547, 16.993754, 17.207118, 17.420638, 17.634316, 17.848151, 18.062145, 18.276298,
            18.490611, 18.705083, 18.919716, 19.134509, 19.349464, 19.564581, 19.77986, 19.995302, 20.210908, 20.426677,
            20.64261, 20.858708, 21.074972, 21.291401, 21.507996, 21.724758, 21.941687, 22.158784, 22.376048, 22.593481,
            22.811083, 23.028855, 23.246796, 23.464907, 23.683189, 23.901643, 24.120267, 24.339064, 24.558033, 24.777175,
            24.996491, 25.21598, 25.435643, 25.655481, 25.875493, 26.095681, 26.316045, 26.536584, 26.757301, 26.978194,
            27.199264, 27.420512, 27.641938, 27.863542, 28.085325, 28.307287, 28.529429, 28.75175, 28.974252, 29.196934,
            29.419796, 29.64284, 29.866065, 30.089472, 30.313061, 30.536833, 30.760787, 30.984924, 31.209244, 31.433748,
            31.658435, 31.883307, 32.108363, 32.333603, 32.559028, 32.784639, 33.010434, 33.236415, 33.462582, 33.688935,
            33.915473, 34.142198, 34.36911, 34.596208, 34.823493, 35.050965, 35.278624, 35.506471, 35.734505, 35.962726,
            36.191135, 36.419732, 36.648517, 36.87749, 37.106651, 37.336, 37.565537, 37.795262, 38.025175, 38.255277,
            38.485567, 38.716046, 38.946712, 39.177567, 39.40861, 39.639842, 39.871261, 40.102869, 40.334664, 40.566648,
            40.798819, 41.031178, 41.263724, 41.496458, 41.729379, 41.962487, 42.195782, 42.429263, 42.662931, 42.896785,
            43.130825, 43.365051, 43.599462, 43.834058, 44.068839, 44.303804, 44.538954, 44.774287, 45.009803, 45.245503,
            45.481384, 45.717448, 45.953693, 46.190119, 46.426726, 46.663513, 46.900479, 47.137624, 47.374947, 47.612448,
            47.850126, 48.08798, 48.32601, 48.564214, 48.802593, 49.041145, 49.279869, 49.518765, 49.757832, 49.997069,
            50.236475, 50.476049, 50.71579, 50.955696, 51.195768, 51.436004, 51.676402, 51.916962, 52.157682, 52.398562,
            52.639599, 52.880793, 53.122142, 53.363644, 53.605299, 53.847105, 54.08906, 54.331162, 54.573411, 54.815804,
            55.058339, 55.301015, 55.543831, 55.786783, 56.02987, 56.27309, 56.516441, 56.759921, 57.003528, 57.247258,
            57.491111, 57.735082, 57.979171, 58.223374, 58.467688, 58.712112, 58.956641, 59.201273, 59.446005, 59.690834,
            59.935756, 60.180768, 60.425868, 60.67105, 60.916312, 61.161649, 61.407058, 61.652534, 61.898074, 62.143672,
            62.389325, 62.635028, 62.880775, 63.126563, 63.372385, 63.618236, 63.864111, 64.110003, 64.355908, 64.601818,
            64.847727, 65.093629, 65.339516, 65.585382, 65.831218, 66.077018, 66.322772, 66.568472, 66.81411, 67.059677,
            67.305162, 67.550557, 67.79585, 68.041031, 68.286089, 68.531011, 68.775786, 69.020402, 69.264843, 69.509098,
            69.75315, 69.996985, 70.240586, 70.483937, 70.727021, 70.969818, 71.21231, 71.454476, 71.696294, 71.937744,
            72.1788, 72.419438, 72.659632, 72.899355, 73.138577, 73.377268, 73.615396, 73.852926, 74.089824, 74.326051,
            74.561566, 74.796327, 75.03029, 75.263406, 75.495626, 75.726895, 75.957156, 76.18635, 76.414413, 76.641275,
            76.866866, 77.091106, 77.313916, 77.535207, 77.754886, 77.972855, 78.189007, 78.403231, 78.615405, 78.825403,
            79.033089, 79.238316, 79.440929, 79.640766, 79.837648, 80.03139, 80.221794, 80.408647, 80.591728, 80.770798,
            80.945609, 81.115895, 81.28138, 81.441772, 81.596768, 81.74605, 81.88929, 82.026151, 82.156285, 82.279339,
            82.394957, 82.502783, 82.602462, 82.693649, 82.77601, 82.849226, 82.913002, 82.967069, 83.011189, 83.045161,
            83.068824, 83.08206, 83.084799, 83.077016, 83.058736, 83.030031, 82.991019, 82.941861, 82.882757, 82.813943,
            82.735685, 82.648278, 82.552034, 82.447284, 82.334368, 82.213633, 82.085432, 81.950114, 81.808025, 81.659505,
            81.504885, 81.344488, 81.178623, 81.007588, 80.831667, 80.651132, 80.466242, 80.27724, 80.084359, 79.887817
        );

        Satellite satellite1 = SatelliteFactory.createSatellite(1, elevationAngles1, amountOfObservations);
        Satellite satellite2 = SatelliteFactory.createSatellite(2, elevationAngles2, amountOfObservations);
        Satellite satellite3 = SatelliteFactory.createSatellite(3, elevationAngles3, amountOfObservations);

        ConsoleOutput consoleOutput = new ConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations, amountOfHours);
        consoleOutput.getConsoleOutput();

        GraphDrawer.draw();
    }
}


class ConsoleOutput {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;
    private final int amountOfObservations;
    private final int amountOfHours;

    public ConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations, int amountOfHours) {
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
        this.amountOfObservations = amountOfObservations;
        this.amountOfHours = amountOfHours;
    }

    public void getConsoleOutput() {
        TemplateConsoleOutput angularVelocitiesOutput = new AngularVelocitiesConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations);
        angularVelocitiesOutput.print();

        TemplateConsoleOutput averageAngularVelocitiesOutput = new AverageAngularVelocitiesConsoleOutput(satellite1, satellite2, satellite3, amountOfHours);
        averageAngularVelocitiesOutput.print();

        TemplateConsoleOutput linearVelocitiesOutput = new LinearVelocitiesConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations);
        linearVelocitiesOutput.print();

        TemplateConsoleOutput averageLinearVelocitiesOutput = new AverageLinearVelocitiesConsoleOutput(satellite1, satellite2, satellite3, amountOfHours);
        averageLinearVelocitiesOutput.print();
    }
}


abstract class TemplateConsoleOutput {
    private final int amountOfObservations;

    public TemplateConsoleOutput(int amountOfObservations) {
        this.amountOfObservations = amountOfObservations;
    }

    public void print() {
        int satellite1Number = getSatellite1Number();
        int satellite2Number = getSatellite2Number();
        int satellite3Number = getSatellite3Number();
        String legend = getLegend();
        System.out.println(legend);
        System.out.println("Спутник #" + satellite1Number + "\t\tСпутник #" + satellite2Number + "\t\tСпутник #" + satellite3Number);
        DecimalFormat df = new DecimalFormat("#.##########");
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double satellite1Velocity = getVelocities1().get(observation);
            double satellite2Velocity = getVelocities2().get(observation);
            double satellite3Velocity = getVelocities3().get(observation);
            System.out.println(df.format(satellite1Velocity) + "\t" + df.format(satellite2Velocity) + "\t" + df.format(satellite3Velocity));
        }
        System.out.println("***********************************************");
    }

    abstract String getLegend();
    abstract int getSatellite1Number();
    abstract int getSatellite2Number();
    abstract int getSatellite3Number();
    abstract ArrayList<Double> getVelocities1();
    abstract ArrayList<Double> getVelocities2();
    abstract ArrayList<Double> getVelocities3();
}


class AngularVelocitiesConsoleOutput extends TemplateConsoleOutput {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;

    public AngularVelocitiesConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(amountOfObservations);
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
    }

    @Override
    protected String getLegend() {
        String legend = "Угловая скорость";
        return legend;
    }

    @Override
    int getSatellite1Number() {
        int number = satellite1.getNumber();
        return number;
    }

    @Override
    int getSatellite2Number() {
        int number = satellite2.getNumber();
        return number;
    }

    @Override
    int getSatellite3Number() {
        int number = satellite3.getNumber();
        return number;
    }

    @Override
    protected ArrayList<Double> getVelocities1() {
        ArrayList<Double> velocities = satellite1.getAngularVelocities();
        return velocities;
    }

    @Override
    protected ArrayList<Double> getVelocities2() {
        ArrayList<Double> velocities = satellite2.getAngularVelocities();
        return velocities;
    }

    @Override
    protected ArrayList<Double> getVelocities3() {
        ArrayList<Double> velocities = satellite3.getAngularVelocities();
        return velocities;
    }
}


class AverageAngularVelocitiesConsoleOutput extends TemplateConsoleOutput {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;

    public AverageAngularVelocitiesConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(amountOfObservations);
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
    }

    @Override
    protected String getLegend() {
        String legend = "Средняя угловая скорость";
        return legend;
    }

    @Override
    int getSatellite1Number() {
        int number = satellite1.getNumber();
        return number;
    }

    @Override
    int getSatellite2Number() {
        int number = satellite2.getNumber();
        return number;
    }

    @Override
    int getSatellite3Number() {
        int number = satellite3.getNumber();
        return number;
    }

    @Override
    protected ArrayList<Double> getVelocities1() {
        ArrayList<Double> velocities = satellite1.getAverageAngularVelocities();
        return velocities;
    }

    @Override
    protected ArrayList<Double> getVelocities2() {
        ArrayList<Double> velocities = satellite2.getAverageAngularVelocities();
        return velocities;
    }

    @Override
    protected ArrayList<Double> getVelocities3() {
        ArrayList<Double> velocities = satellite3.getAverageAngularVelocities();
        return velocities;
    }
}


class LinearVelocitiesConsoleOutput extends TemplateConsoleOutput {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;

    public LinearVelocitiesConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(amountOfObservations);
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
    }

    @Override
    protected String getLegend() {
        String legend = "Линейная скорость";
        return legend;
    }

    @Override
    int getSatellite1Number() {
        int number = satellite1.getNumber();
        return number;
    }

    @Override
    int getSatellite2Number() {
        int number = satellite2.getNumber();
        return number;
    }

    @Override
    int getSatellite3Number() {
        int number = satellite3.getNumber();
        return number;
    }

    @Override
    protected ArrayList<Double> getVelocities1() {
        ArrayList<Double> velocities = satellite1.getLinearVelocities();
        return velocities;
    }

    @Override
    protected ArrayList<Double> getVelocities2() {
        ArrayList<Double> velocities = satellite2.getLinearVelocities();
        return velocities;
    }

    @Override
    protected ArrayList<Double> getVelocities3() {
        ArrayList<Double> velocities = satellite3.getLinearVelocities();
        return velocities;
    }
}


class AverageLinearVelocitiesConsoleOutput extends TemplateConsoleOutput {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;

    public AverageLinearVelocitiesConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(amountOfObservations);
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
    }

    @Override
    protected String getLegend() {
        String legend = "Средняя линейная скорость";
        return legend;
    }

    @Override
    int getSatellite1Number() {
        int number = satellite1.getNumber();
        return number;
    }

    @Override
    int getSatellite2Number() {
        int number = satellite2.getNumber();
        return number;
    }

    @Override
    int getSatellite3Number() {
        int number = satellite3.getNumber();
        return number;
    }

    @Override
    protected ArrayList<Double> getVelocities1() {
        ArrayList<Double> velocities = satellite1.getAverageLinearVelocities();
        return velocities;
    }

    @Override
    protected ArrayList<Double> getVelocities2() {
        ArrayList<Double> velocities = satellite2.getAverageLinearVelocities();
        return velocities;
    }

    @Override
    protected ArrayList<Double> getVelocities3() {
        ArrayList<Double> velocities = satellite3.getAverageLinearVelocities();
        return velocities;
    }
}


class SatelliteFactory {
    public static Satellite createSatellite(int number, List<Double> elevationAnglesArr, int amountOfObservations) {
        ElevationAngles elevationAngles = new ElevationAngles(elevationAnglesArr);
        AngularVelocities angularVelocities = new AngularVelocities(elevationAngles, amountOfObservations);
        LinearVelocities linearVelocities = new LinearVelocities(amountOfObservations);
        Satellite satellite = new Satellite(number, angularVelocities, linearVelocities);
        return satellite;
    }
}


class Satellite {
    private final int number;
    private final AngularVelocities angularVelocities;
    private final LinearVelocities linearVelocities;

    public Satellite(int number, AngularVelocities angularVelocities, LinearVelocities linearVelocities) {
        this.number = number;
        this.angularVelocities = angularVelocities;
        this.linearVelocities = linearVelocities;
    }

    public int getNumber() {
        return number;
    }

    public ArrayList<Double> getAngularVelocities() {
        ArrayList<Double> velocities = angularVelocities.getAngularVelocities();
        return velocities;
    }

    public ArrayList<Double> getAverageAngularVelocities() {
        ArrayList<Double> velocities = angularVelocities.getAverageAngularVelocities();
        return velocities;
    }

    public ArrayList<Double> getLinearVelocities() {
        ArrayList<Double> velocities = linearVelocities.getLinearVelocities();
        return velocities;
    }

    public ArrayList<Double> getAverageLinearVelocities() {
        ArrayList<Double> velocities = linearVelocities.getAverageLinearVelocities();
        return velocities;
    }
}


class AngularVelocities {
    private final int amountOfObservations;
    private final ElevationAngles elevationAngles;
    private final ArrayList<Double> angularVelocities;

    public AngularVelocities(ElevationAngles elevationAngles, int amountOfObservations) {
        this.amountOfObservations = amountOfObservations;
        this.angularVelocities = new ArrayList<>(amountOfObservations);
        this.elevationAngles = elevationAngles;
    }

    public ArrayList<Double> getAverageAngularVelocities() {
        ArrayList<Double> averageVelocities = new ArrayList<>();
        ArrayList<Double> angularVelocities = getAngularVelocities();
        int observationsPerHour = 120;

        double firstHourSum = getVelocitySumPerHour(angularVelocities, 0, 120);
        double secondHourSum = getVelocitySumPerHour(angularVelocities, 120, 240);
        double thirdHourSum = getVelocitySumPerHour(angularVelocities, 240, 360);

        double firstHourAverage = firstHourSum / observationsPerHour;
        double secondHourAverage = secondHourSum / observationsPerHour;
        double thirdHourAverage = thirdHourSum / observationsPerHour;

        averageVelocities.add(firstHourAverage);
        averageVelocities.add(secondHourAverage);
        averageVelocities.add(thirdHourAverage);
        return averageVelocities;
    }

    public ArrayList<Double> getAngularVelocities() {
        double oneHourInSeconds = 3600;
        double previousElevationAngle = elevationAngles.getAngleAt(0);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double currentElevationAngle = elevationAngles.getAngleAt(observation);
            double angularVelocity = (currentElevationAngle - previousElevationAngle) / oneHourInSeconds;
            angularVelocities.add(angularVelocity);
            previousElevationAngle = currentElevationAngle;
        }
        return angularVelocities;
    }

    private double getVelocitySumPerHour(ArrayList<Double> velocities, int start, int end) {
        double velocitySum = 0;
        for (int observation = start; observation < end; observation++) {
            velocitySum += velocities.get(observation);
        }
        return velocitySum;
    }
}


class LinearVelocities {
    private final ArrayList<Double> linearVelocities;
    private final int amountOfObservations;

    public LinearVelocities(int amountOfObservations) {
        this.amountOfObservations = amountOfObservations;
        this.linearVelocities = new ArrayList<>(amountOfObservations);
    }

    ArrayList<Double> getAverageLinearVelocities() {
        ArrayList<Double> averageVelocities = new ArrayList<>();
        ArrayList<Double> linearVelocities = getLinearVelocities();
        int observationsPerHour = 120;

        double firstHourSum = getVelocitySumPerHour(linearVelocities, 0, 120);
        double secondHourSum = getVelocitySumPerHour(linearVelocities, 120, 240);
        double thirdHourSum = getVelocitySumPerHour(linearVelocities, 240, 360);

        double firstHourAverage = firstHourSum / observationsPerHour;
        double secondHourAverage = secondHourSum / observationsPerHour;
        double thirdHourAverage = thirdHourSum / observationsPerHour;

        averageVelocities.add(firstHourAverage);
        averageVelocities.add(secondHourAverage);
        averageVelocities.add(thirdHourAverage);

        return averageVelocities;
    }

    public ArrayList<Double> getLinearVelocities() {
        double gravitational = 6.67 * Math.pow(10, -11);
        double earthMass = 5.972E24;
        double earthRadius = 6_371_000;
        double height = 20_000;

        for (int observation = 0; observation < amountOfObservations; observation++) {
            double linearVelocity = Math.sqrt(gravitational * earthMass / (earthRadius + height));
            linearVelocities.add(linearVelocity);
        }
        return linearVelocities;
    }

    private double getVelocitySumPerHour(ArrayList<Double> velocities, int start, int end) {
        double hourSum = 0;
        for (int observation = start; observation < end; observation++) {
            hourSum += velocities.get(observation);
        }
        return hourSum;
    }
}


class ElevationAngles {
    private final List<Double> elevationAngles;

    public ElevationAngles(List<Double> elevationAngles) {
        this.elevationAngles = elevationAngles;
    }

    public double getAngleAt(int observation) {
        return elevationAngles.get(observation);
    }
}